package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SuccessSubCategories {

    @SerializedName("msg")
    private String msg;
    @SerializedName("success")
    private int success;

    @SerializedName("infoData")
    private List<MSubCategories> subcategories;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<MSubCategories> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<MSubCategories> subcategories) {
        this.subcategories = subcategories;
    }
}
