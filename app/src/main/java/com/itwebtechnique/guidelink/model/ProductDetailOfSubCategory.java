package com.itwebtechnique.guidelink.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ProductDetailOfSubCategory {


    @SerializedName("id")
    private int id;
    @SerializedName("UserId")
    private int UserId;
    @SerializedName("FullName")
    private String FullName;
    @SerializedName("MobileNo")
    private String MobileNo;
    @SerializedName("EmailAddress")
    private String EmailAddress;
    @SerializedName("ShopName")
    private String ShopName;
    @SerializedName("Logo")
    private String Logo;
    @SerializedName("LandlineNo")
    private String LandlineNo;
    @SerializedName("FaxNumber")
    private String FaxNumber;
    @SerializedName("Country")
    private String Country;
    @SerializedName("Location")
    private String Location;
    @SerializedName("Address")
    private String Address;
    @SerializedName("WebsiteURL")
    private String WebsiteURL;
    @SerializedName("Description")
    private String Description;
    @SerializedName("Latitude")
    private String Latitude;
    @SerializedName("Longitude")
    private String Longitude;

    @SerializedName("HeadingName")
    private String HeadingName;
    @SerializedName("PhoneNo")
    private String PhoneNo;
    @SerializedName("Title")
    private String Title;
    @SerializedName("PostId")
    private int PostId;




    public int getPostId() {
        return PostId;
    }

    public void setPostId(int postId) {
        PostId = postId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }



    public String getHeadingName() {
        return HeadingName;
    }

    public void setHeadingName(String headingName) {
        HeadingName = headingName;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public String getLogo() {
        return Logo;
    }

    public void setLogo(String logo) {
        Logo = logo;
    }

    public String getLandlineNo() {
        return LandlineNo;
    }

    public void setLandlineNo(String landlineNo) {
        LandlineNo = landlineNo;
    }

    public String getFaxNumber() {
        return FaxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        FaxNumber = faxNumber;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getWebsiteURL() {
        return WebsiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        WebsiteURL = websiteURL;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }


}
