package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SuccessLocationSuggetion {


    @SerializedName("success")
    private int success;

    @SerializedName("msg")
    private String msg;
    @SerializedName("infoData")
    private List<MLocationSuggestion> loationsuggestion;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<MLocationSuggestion> getLoationsuggestion() {
        return loationsuggestion;
    }

    public void setLoationsuggestion(List<MLocationSuggestion> loationsuggestion) {
        this.loationsuggestion = loationsuggestion;
    }
}
