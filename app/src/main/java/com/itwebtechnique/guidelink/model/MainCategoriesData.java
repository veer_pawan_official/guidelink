package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainCategoriesData {

    @SerializedName("data")
    private List<MAllCategories> infoData;

    public List<MAllCategories> getInfoData() {
        return infoData;
    }

    public void setInfoData(List<MAllCategories> infoData) {
        this.infoData = infoData;
    }
}
