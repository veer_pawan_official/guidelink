package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

public class UserInfoData {

    @SerializedName("UserId")
    private int UserId;

    @SerializedName("FullName")
    private String FullName;

    @SerializedName("FirstName")
    private String FirstName;

    @SerializedName("MobileNo")
    private String MobileNo;

    @SerializedName("EmailAddress")
    private String EmailAddress;

    @SerializedName("UserType")
    private int UserType;

    @SerializedName("BusinessCode")
    private String BusinessCode;

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public String getBusinessCode() {
        return BusinessCode;
    }

    public void setBusinessCode(String businessCode) {
        BusinessCode = businessCode;
    }
}
