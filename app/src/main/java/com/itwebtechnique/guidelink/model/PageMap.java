package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PageMap {
    @SerializedName("cse_thumbnail")
    List<CseThumbnail> thumbnailList;

    public List<CseThumbnail> getThumbnailList() {
        return thumbnailList;
    }

    public void setThumbnailList(List<CseThumbnail> thumbnailList) {
        this.thumbnailList = thumbnailList;
    }
}
