package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

public class GoogleResultsItems {


    @SerializedName("snippet")
    private String snippet;

    @SerializedName("title")
    private String title;

    @SerializedName("htmlTitle")
    private String htmlTitle;

    @SerializedName("displayLink")
    private String displayLink;

    @SerializedName("cacheId")
    private String cacheId;
    @SerializedName("link")
    private String link;
    @SerializedName("pagemap")
    PageMap pageMapList;

    public PageMap getPageMapList() {
        return pageMapList;
    }

    public void setPageMapList(PageMap pageMapList) {
        this.pageMapList = pageMapList;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHtmlTitle() {
        return htmlTitle;
    }

    public void setHtmlTitle(String htmlTitle) {
        this.htmlTitle = htmlTitle;
    }

    public String getDisplayLink() {
        return displayLink;
    }

    public void setDisplayLink(String displayLink) {
        this.displayLink = displayLink;
    }

    public String getCacheId() {
        return cacheId;
    }

    public void setCacheId(String cacheId) {
        this.cacheId = cacheId;
    }
}
