package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SuccessAllCategories {

    @SerializedName("msg")
    private String msg;
    @SerializedName("success")
    private int success;
   /* @SerializedName("userInfoData")
    private List<UserInfoData> userInfoData;
    @SerializedName("VerifyCode")
    private String VerifyCode;
*/
    @SerializedName("infoData")

    private MainCategoriesData maincategoriesData;

   //


    public MainCategoriesData getMaincategoriesData() {
        return maincategoriesData;
    }

    public void setMaincategoriesData(MainCategoriesData maincategoriesData) {
        this.maincategoriesData = maincategoriesData;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

   /* public List<UserInfoData> getUserInfoData() {
        return userInfoData;
    }

    public void setUserInfoData(List<UserInfoData> userInfoData) {
        this.userInfoData = userInfoData;
    }

    public String getVerifyCode() {
        return VerifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        VerifyCode = verifyCode;
    }*/


}
