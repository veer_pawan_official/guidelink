package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SuccessInnerSubCategories {

    @SerializedName("msg")
    private String msg;

    @SerializedName("success")
    private int success;

    @SerializedName("infoData")
    private List<MInnerSubCategories> infoData;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<MInnerSubCategories> getInfoData() {
        return infoData;
    }

    public void setInfoData(List<MInnerSubCategories> infoData) {
        this.infoData = infoData;
    }
}
