package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

public class MLocationSuggestion {

    @SerializedName("PostecodeCity")
    private String PostecodeCity;

    public String getPostecodeCity() {
        return PostecodeCity;
    }

    public void setPostecodeCity(String postecodeCity) {
        PostecodeCity = postecodeCity;
    }
}
