package com.itwebtechnique.guidelink.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class MAllCategories implements Parcelable {


    @SerializedName("CatName")
    private String CatName;
    @SerializedName("id")
    private int id;
    @SerializedName("isSubCategoryAvailable")
    private int isSubCategoryAvailable;
    @SerializedName("CatIcon")
    private String CatIcon;
    @SerializedName("Banner")
    private String Banner;


    public int getIsSubCategoryAvailable() {
        return isSubCategoryAvailable;
    }

    public void setIsSubCategoryAvailable(int isSubCategoryAvailable) {
        this.isSubCategoryAvailable = isSubCategoryAvailable;
    }


    protected MAllCategories(Parcel in) {
        CatName = in.readString();
        id = in.readInt();
        CatIcon = in.readString();
        Banner = in.readString();
        isSubCategoryAvailable=in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CatName);
        dest.writeInt(id);
        dest.writeString(CatIcon);
        dest.writeString(Banner);
        dest.writeInt(isSubCategoryAvailable);
    }

    public static final Creator<MAllCategories> CREATOR = new Creator<MAllCategories>() {
        @Override
        public MAllCategories createFromParcel(Parcel in) {
            return new MAllCategories(in);
        }

        @Override
        public MAllCategories[] newArray(int size) {
            return new MAllCategories[size];
        }
    };



    public String getCatName() {
        return CatName;
    }

    public void setCatName(String catName) {
        CatName = catName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCatIcon() {
        return CatIcon;
    }

    public void setCatIcon(String catIcon) {
        CatIcon = catIcon;
    }

    public String getBanner() {
        return Banner;
    }

    public void setBanner(String banner) {
        Banner = banner;
    }

    @Override
    public int describeContents() {
        return 0;
    }



}
