package com.itwebtechnique.guidelink.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class MSubCategories implements Parcelable {
    @SerializedName("id")
    private int id;

    @SerializedName("SubCatName")
    private String subCatName;

    @SerializedName("subCatIcon")
    private String subCatIcon;

    @SerializedName("IsInnerSubCategoryAvailbale")
    private int IsInnerSubCategoryAvailbale;


    protected MSubCategories(Parcel in) {
        id = in.readInt();
        subCatName = in.readString();
        subCatIcon = in.readString();
        IsInnerSubCategoryAvailbale = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(subCatName);
        dest.writeString(subCatIcon);
        dest.writeInt(IsInnerSubCategoryAvailbale);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MSubCategories> CREATOR = new Creator<MSubCategories>() {
        @Override
        public MSubCategories createFromParcel(Parcel in) {
            return new MSubCategories(in);
        }

        @Override
        public MSubCategories[] newArray(int size) {
            return new MSubCategories[size];
        }
    };

    public int getIsInnerSubCategoryAvailbale() {
        return IsInnerSubCategoryAvailbale;
    }

    public void setIsInnerSubCategoryAvailbale(int isInnerSubCategoryAvailbale) {
        IsInnerSubCategoryAvailbale = isInnerSubCategoryAvailbale;
    }

    public String getSubCatIcon() {
        return subCatIcon;
    }

    public void setSubCatIcon(String subCatIcon) {
        this.subCatIcon = subCatIcon;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }



}
