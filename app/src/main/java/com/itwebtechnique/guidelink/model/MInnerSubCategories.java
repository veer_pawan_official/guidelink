package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

public class MInnerSubCategories {

    @SerializedName("id")
    private int id;

    @SerializedName("SubCatName")
    private String SubCatName;


    @SerializedName("subCatName")
    private String alt_subCatName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubCatName() {
        return SubCatName;
    }

    public void setSubCatName(String subCatName) {
        SubCatName = subCatName;
    }

    public String getAlt_subCatName() {
        return alt_subCatName;
    }

    public void setAlt_subCatName(String alt_subCatName) {
        this.alt_subCatName = alt_subCatName;
    }
}
