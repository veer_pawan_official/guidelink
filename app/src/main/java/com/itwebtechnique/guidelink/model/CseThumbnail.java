package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

public class CseThumbnail {

    @SerializedName("src")
    private String src;

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
}
