package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GoogleResults {
    @SerializedName("items")
    private List<GoogleResultsItems> itesm;

    public List<GoogleResultsItems> getItesm() {
        return itesm;
    }

    public void setItesm(List<GoogleResultsItems> itesm) {
        this.itesm = itesm;
    }
}
