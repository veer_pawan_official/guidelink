package com.itwebtechnique.guidelink.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductData {

    @SerializedName("data")
    private List<ProductDetailOfSubCategory> subCategories;
    @SerializedName("next_page_url")
    private String next_page_url;

    public List<ProductDetailOfSubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<ProductDetailOfSubCategory> subCategories) {
        this.subCategories = subCategories;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }
}
