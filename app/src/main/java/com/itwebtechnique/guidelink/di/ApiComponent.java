package com.itwebtechnique.guidelink.di;

import com.itwebtechnique.guidelink.view.Cars;
import com.itwebtechnique.guidelink.view.CatSubcategoriesProducts;
import com.itwebtechnique.guidelink.view.ForgetPassword;
import com.itwebtechnique.guidelink.view.HomeCategoriesGrid;
import com.itwebtechnique.guidelink.view.HomeCategoriesList;
import com.itwebtechnique.guidelink.view.House;
import com.itwebtechnique.guidelink.view.Login;
import com.itwebtechnique.guidelink.view.Registration;
import com.itwebtechnique.guidelink.view.SubCategory;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface ApiComponent {

    void inject(Login login);
    void inject(SubCategory subCategory);

    void inject(CatSubcategoriesProducts catSubcategoriesProducts);

    void inject(HomeCategoriesGrid homeCategoriesGrid);

    void inject(Registration registration);

    void inject(ForgetPassword forgetPassword);

    void inject(House house);

    void inject(Cars cars);

    void inject(HomeCategoriesList homeCategoriesList);
    //void inject(SubCategory subCategory);
}
