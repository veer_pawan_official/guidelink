package com.itwebtechnique.guidelink.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.model.MAllCategories;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoriesListAdapter extends RecyclerView.Adapter<CategoriesListAdapter.MyViewHolder> {

    Context mCtx;
    List<MAllCategories> MAllCategoriesList;

    public CategoriesListAdapter(Context mCtx, List<MAllCategories> MAllCategoriesList) {
        this.mCtx = mCtx;
        this.MAllCategoriesList = MAllCategoriesList;
    }


    @NonNull
    @Override
    public CategoriesListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new CategoriesListAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_list_items, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesListAdapter.MyViewHolder holder, int position) {


        Picasso.with(mCtx)
                .load(MAllCategoriesList.get(position).getCatIcon())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.imageView);

/*
        Glide.with(mCtx)
                .load(MAllCategoriesList.get(position).getCatIcon())
                .into(holder.imageView);
        Toast.makeText(mCtx,MAllCategoriesList.get(position).getId()+"",Toast.LENGTH_LONG).show();*/

        holder.textView.setText(MAllCategoriesList.get(position).getCatName());
    }

    @Override
    public int getItemCount() {
        return MAllCategoriesList.size();
        //return Math.min(MAllCategoriesList.size(), 9);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;

        public MyViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
        }
    }
}