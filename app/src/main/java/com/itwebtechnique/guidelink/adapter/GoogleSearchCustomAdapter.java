package com.itwebtechnique.guidelink.adapter;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.model.GoogleResultsItems;
import com.itwebtechnique.guidelink.model.MAllCategories;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GoogleSearchCustomAdapter extends RecyclerView.Adapter<GoogleSearchCustomAdapter.MyViewHolder> {

    Context mCtx;
    List<GoogleResultsItems> MAllCategoriesList;

    public GoogleSearchCustomAdapter(Context mCtx, List<GoogleResultsItems> MAllCategoriesList) {
        this.mCtx = mCtx;
        this.MAllCategoriesList = MAllCategoriesList;
    }


    @NonNull
    @Override
    public GoogleSearchCustomAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new GoogleSearchCustomAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.google_search_list_items, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull GoogleSearchCustomAdapter.MyViewHolder holder, final int position) {


     /*   Picasso.with(mCtx)
                .load(MAllCategoriesList.get(position).getPageMapList().getThumbnailList().get(0).getSrc())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.img_google_search);*/
        //Log.e("FGTH", MAllCategoriesList.get(position).getPageMapList().getThumbnailList().get(position).getSrc() + "");
        holder.tv_link.setText(MAllCategoriesList.get(position).getLink());
        holder.tv_title.setText(MAllCategoriesList.get(position).getTitle());
        holder.tv_snippet.setText(MAllCategoriesList.get(position).getSnippet());
    }

    @Override
    public int getItemCount() {
        return MAllCategoriesList.size();
        //return Math.min(MAllCategoriesList.size(), 9);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tv_snippet;
        TextView tv_title;
        TextView tv_link;
        ImageView img_google_search;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_snippet = itemView.findViewById(R.id.tv_snippet_name);
            tv_title = itemView.findViewById(R.id.tv_title_name);
            tv_link = itemView.findViewById(R.id.tv_link);
            img_google_search = itemView.findViewById(R.id.img_google_search);
        }
    }
}
