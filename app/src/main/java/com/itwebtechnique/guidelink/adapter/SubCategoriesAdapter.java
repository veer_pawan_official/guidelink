package com.itwebtechnique.guidelink.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.model.MSubCategories;
import com.itwebtechnique.guidelink.view.SubCategory;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCategoriesAdapter extends RecyclerView.Adapter<SubCategoriesAdapter.MyViewHolder> {

    private Context mCtx;
    private List<MSubCategories> MCategoriesList;

    public SubCategoriesAdapter(Context mCtx, List<MSubCategories> MCategoriesList) {
        this.mCtx = mCtx;
        this.MCategoriesList = MCategoriesList;
    }



    @NonNull
    @Override
    public SubCategoriesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new SubCategoriesAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(SubCategory.isViewWithCatalog ? R.layout.sub_categories_grid_items: R.layout.sub_categories_list_items, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoriesAdapter.MyViewHolder holder, int position) {



        Picasso.with(mCtx)
                .load(MCategoriesList.get(position).getSubCatIcon())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.img_sub_category);

        holder.tv_category_name.setText(MCategoriesList.get(position).getSubCatName());

    }

    @Override
    public int getItemCount() {
        return MCategoriesList.size();
        //return Math.min(MAllCategoriesList.size(), 9);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_category_name)
        TextView tv_category_name;
        @BindView(R.id.img_sub_category)
        ImageView img_sub_category;



        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
