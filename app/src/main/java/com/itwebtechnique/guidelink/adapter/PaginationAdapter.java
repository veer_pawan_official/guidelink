package com.itwebtechnique.guidelink.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.model.ProductDetailOfSubCategory;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Suleiman on 19/10/16.
 */

public class PaginationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;


    private List<ProductDetailOfSubCategory> categoriesList;
    private Context context;
    private int category_id;

    private boolean isLoadingAdded = false;

    public PaginationAdapter(Context context) {
        this.context = context;
        categoriesList = new ArrayList<>();

    }

    public List<ProductDetailOfSubCategory> getMovies() {
        return categoriesList;
    }

    public void setMovies(List<ProductDetailOfSubCategory> movieResults) {
        this.categoriesList = movieResults;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.sub_categories_product_list_items, parent, false);
        viewHolder = new MovieVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ProductDetailOfSubCategory result = categoriesList.get(position); // Movie

        switch (getItemViewType(position)) {
            case ITEM:
                final MovieVH movieVH = (MovieVH) holder;

               /* if (result.getLogo().contains(".svg")) {


                    Picasso.with(context)
                            .load(R.drawable.placeholder)
                            .placeholder(R.drawable.placeholder)
                            .resize(200, 200)
                            .error(R.drawable.placeholder)
                            .into(movieVH.img_cetagory_icon);


                }
*/

                Picasso.with(context)
                        .load(result.getLogo())
                        .placeholder(R.drawable.placeholder)

                        .skipMemoryCache()
                        .error(R.drawable.placeholder)
                        .into(movieVH.img_cetagory_icon);


                if (category_id == 27) {
                    movieVH.tv_category_address.setVisibility(View.GONE);
                    movieVH.tv_category_name.setText(result.getTitle());
                    movieVH.tv_category_name.setGravity(Gravity.CENTER_VERTICAL);
                    movieVH.tv_category_contact_number.setVisibility(View.GONE);

                }


                if (category_id == 4) {

                    movieVH.tv_category_name.setText(result.getShopName());
                    movieVH.tv_category_address.setGravity(Gravity.CENTER_VERTICAL);
                    movieVH.tv_category_address.setVisibility(View.GONE);
                    movieVH.tv_category_contact_number.setVisibility(View.GONE);
                }


                if (category_id == 23) {
                    movieVH.tv_category_address.setVisibility(View.GONE);
                    movieVH.tv_category_name.setText(result.getHeadingName());
                    movieVH.tv_category_contact_number.setText(categoriesList.get(position).getPhoneNo());
                    movieVH.tv_category_contact_number.setInputType(InputType.TYPE_CLASS_PHONE);
                    movieVH.tv_category_contact_number.setTextSize(15);


                }


                if (category_id == 5 || category_id == 21) {
                    movieVH.tv_category_name.setText(result.getFullName());
                    movieVH.tv_category_address.setText(result.getAddress());
                    movieVH.tv_category_contact_number.setText(result.getMobileNo());

                }


                if (category_id == 7 || category_id == 8 || category_id == 9 || category_id == 17 || category_id == 26 || category_id == 14 || category_id == 11 || category_id == 19 || category_id == 20 || category_id == 13 || category_id == 12 || category_id == 15 || category_id == 6 || category_id == 25 || category_id == 18 || category_id == 24 || category_id == 1 || category_id == 10) {
                    movieVH.tv_category_name.setText(result.getShopName());
                    movieVH.tv_category_address.setText(result.getAddress());
                    movieVH.tv_category_contact_number.setText(result.getMobileNo());
                    if (categoriesList.get(position).getMobileNo().equals("")) {
                        movieVH.tv_category_contact_number.setText("Not Available");
                    } else {
                        movieVH.tv_category_contact_number.setText(result.getMobileNo());

                    }
                    if (categoriesList.get(position).getAddress().equals("")) {

                        movieVH.tv_category_address.setText(result.getLocation() + result.getCountry());

                    } else {
                        movieVH.tv_category_address.setText(result.getAddress() + "  " + result.getLocation());

                    }

                }


                break;

            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {


        //return  categoriesList.size();
        return categoriesList == null ? 0 : categoriesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == categoriesList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(ProductDetailOfSubCategory r) {
        categoriesList.add(r);
        notifyItemInserted(categoriesList.size() - 1);
    }

    public void addAll(List<ProductDetailOfSubCategory> moveResults, int category_idd) {
        category_id = category_idd;
        for (ProductDetailOfSubCategory result : moveResults) {
            add(result);
        }
    }

    public void remove(ProductDetailOfSubCategory r) {
        int position = categoriesList.indexOf(r);
        if (position > -1) {
            categoriesList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ProductDetailOfSubCategory());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = categoriesList.size() - 1;
        ProductDetailOfSubCategory result = getItem(position);

        if (result != null) {
            categoriesList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public ProductDetailOfSubCategory getItem(int position) {
        return categoriesList.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    public class MovieVH extends RecyclerView.ViewHolder {
        @BindView(R.id.img_cetagory_icon)
        ImageView img_cetagory_icon;
        @BindView(R.id.tv_category_name)
        TextView tv_category_name;
        @BindView(R.id.tv_category_address)
        TextView tv_category_address;

        @BindView(R.id.tv_category_contact_number)
        TextView tv_category_contact_number;

        public MovieVH(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


}
