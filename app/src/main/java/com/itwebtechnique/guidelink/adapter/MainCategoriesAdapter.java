package com.itwebtechnique.guidelink.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.model.MAllCategories;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


import java.io.ByteArrayOutputStream;
import java.util.List;

public class MainCategoriesAdapter extends RecyclerView.Adapter<MainCategoriesAdapter.MyViewHolder> {

    Context mCtx;
    List<MAllCategories> MAllCategoriesList;

    public MainCategoriesAdapter(Context mCtx, List<MAllCategories> MAllCategoriesList) {
        this.mCtx = mCtx;
        this.MAllCategoriesList = MAllCategoriesList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.main_categories_items, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
/*
        Bitmap bm = BitmapFactory.decodeFile(MAllCategoriesList.get(position).getCatIcon());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        Toast.makeText(mCtx,b.toString(),Toast.LENGTH_LONG).show();*/

        Picasso.with(mCtx)
                .load(MAllCategoriesList.get(position).getCatIcon())
                /*.placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)*/
                .into(holder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });

/*
        Glide.with(mCtx)
                .load(MAllCategoriesList.get(position).getCatIcon())
                .into(holder.imageView);
        Toast.makeText(mCtx,MAllCategoriesList.get(position).getId()+"",Toast.LENGTH_LONG).show();*/

        holder.textView.setText(MAllCategoriesList.get(position).getCatName());
    }

    @Override
    public int getItemCount() {
        return MAllCategoriesList.size();
        //return Math.min(MAllCategoriesList.size(), 26);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;
        ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            progressBar=itemView.findViewById(R.id.homeprogress);
        }
    }
}