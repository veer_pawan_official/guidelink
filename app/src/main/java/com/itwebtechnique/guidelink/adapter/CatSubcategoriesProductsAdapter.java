package com.itwebtechnique.guidelink.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.model.ProductDetailOfSubCategory;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CatSubcategoriesProductsAdapter extends RecyclerView.Adapter<CatSubcategoriesProductsAdapter.MyViewHolder> {

    private static final int MAX_WIDTH = 96;
    private static final int MAX_HEIGHT = 96;
    private Context mCtx;
    private List<ProductDetailOfSubCategory> categoriesList;
    private int category_id;

    public CatSubcategoriesProductsAdapter(Context mCtx, List<ProductDetailOfSubCategory> categoriesList, int category_id) {
        this.mCtx = mCtx;
        this.categoriesList = categoriesList;
        this.category_id = category_id;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_categories_product_list_items, parent, false));

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        if (categoriesList.get(position).getLogo().contains(".svg")) {


            Picasso.with(mCtx)
                    .load(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .resize(200, 200)
                    .error(R.drawable.placeholder)
                    .into(holder.img_cetagory_icon);


        }


        Picasso.with(mCtx)
                .load(categoriesList.get(position).getLogo())
                .placeholder(R.drawable.placeholder)

                .skipMemoryCache()
                .error(R.drawable.placeholder)
                .into(holder.img_cetagory_icon);


        if (category_id == 27) {
            holder.tv_category_address.setVisibility(View.GONE);
            holder.tv_category_name.setText(categoriesList.get(position).getTitle());
            holder.tv_category_name.setGravity(Gravity.CENTER_VERTICAL);
            holder.tv_category_contact_number.setVisibility(View.GONE);

        }


        if (category_id == 4) {

            holder.tv_category_name.setText(categoriesList.get(position).getShopName());
            holder.tv_category_address.setGravity(Gravity.CENTER_VERTICAL);
            holder.tv_category_address.setVisibility(View.GONE);
            holder.tv_category_contact_number.setVisibility(View.GONE);
        }


        if (category_id == 23) {
            holder.tv_category_address.setVisibility(View.GONE);
            holder.tv_category_name.setText(categoriesList.get(position).getHeadingName());
            holder.tv_category_contact_number.setText(categoriesList.get(position).getPhoneNo());
            holder.tv_category_contact_number.setInputType(InputType.TYPE_CLASS_PHONE);
            holder.tv_category_contact_number.setTextSize(15);


        }


        if (category_id == 5||category_id == 21) {
            holder.tv_category_name.setText(categoriesList.get(position).getFullName());
            holder.tv_category_address.setText(categoriesList.get(position).getAddress());
            holder.tv_category_contact_number.setText(categoriesList.get(position).getMobileNo());

        }



        if (category_id == 7||category_id == 8 || category_id == 9 || category_id == 17 || category_id == 26 || category_id == 14 || category_id == 11 || category_id == 19 || category_id == 20 || category_id == 13 || category_id == 12 || category_id == 15 || category_id == 6 || category_id == 25 || category_id == 18 || category_id == 24 || category_id == 1 || category_id == 10) {
            holder.tv_category_name.setText(categoriesList.get(position).getShopName());
            holder.tv_category_address.setText(categoriesList.get(position).getAddress());
            holder.tv_category_contact_number.setText(categoriesList.get(position).getMobileNo());
            if (categoriesList.get(position).getMobileNo().equals("")) {
                holder.tv_category_contact_number.setText("Not Available");
            } else {
                holder.tv_category_contact_number.setText(categoriesList.get(position).getMobileNo());

            }
            if (categoriesList.get(position).getAddress().equals("")) {

                holder.tv_category_address.setText(categoriesList.get(position).getLocation() + categoriesList.get(position).getCountry());

            } else {
                holder.tv_category_address.setText(String.format("%s", categoriesList.get(position).getAddress() + " " +System.lineSeparator()+ categoriesList.get(position).getLocation()));

            }

        }


    }

    @Override
    public int getItemCount() {
        //return categoriesList.size();
        return Math.min(categoriesList.size(), 100);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_cetagory_icon)
        ImageView img_cetagory_icon;
        @BindView(R.id.tv_category_name)
        TextView tv_category_name;
        @BindView(R.id.tv_category_address)
        TextView tv_category_address;

        @BindView(R.id.tv_category_contact_number)
        TextView tv_category_contact_number;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}