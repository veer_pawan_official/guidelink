package com.itwebtechnique.guidelink.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.app.MyApplication;
import com.itwebtechnique.guidelink.model.MSubCategories;
import com.itwebtechnique.guidelink.viewmodel.RemoteData;

import com.weiwangcn.betterspinner.library.BetterSpinner;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Retrofit;

public class House extends BaseActivity {

    @Inject
    Retrofit retrofit;
    @BindView(R.id.tv_current_location)
    TextView tv_current_location;
    Spinner sp_sub_categories;
    Spinner sp_distance;

    BetterSpinner sp_property_type, sp_chf_min, sp_chf_max, sp_room_min, sp_room_max, sp_living_space_min, sp_living_space_max;
    String str_property_type, str_chf_min, str_chf_max, str_room_min, str_room_max, str_living_space_min, str_living_space_max;
    ImageView img_category_banner;
    List<MSubCategories> sub_categories;
    ArrayList<String> sub_categories_values = new ArrayList<>();
    ArrayList<Integer> sub_categories_ids = new ArrayList<Integer>();
    private String sub_category_name;
    private String distance_in_km = "5";
    private String location = HomeCategoriesGrid.current_location;
    private int category_id;
    private TextView errorText;
    private ProgressBar loader;
    private int sub_category_id, subcategory_inner_id, inner_sub_categories_id;
    private AutoCompleteTextView storeTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house);
        ButterKnife.bind(this);
        initials();


        setSpinnerAdapters();
        storeTV.setOnItemClickListener((parent, view, position, id) -> {

            location = parent.getItemAtPosition(position).toString();

            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        });
        sp_distance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                distance_in_km = item.replaceAll("[^0-9]", "");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_sub_categories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sub_category_id = sub_categories_ids.get(position);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        sp_property_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), sp_property_type.getText()+"  "+position, Toast.LENGTH_SHORT).show();

            }
        });


        sp_chf_min.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), sp_chf_min.getText()+"  "+position, Toast.LENGTH_SHORT).show();

            }
        });

        sp_chf_max.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), sp_chf_max.getText()+"  "+position, Toast.LENGTH_SHORT).show();

            }
        });

        sp_room_min.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), sp_room_min.getText()+"  "+position, Toast.LENGTH_SHORT).show();

            }
        });

        sp_room_max.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), sp_room_max.getText()+"  "+position, Toast.LENGTH_SHORT).show();

            }
        });


        sp_living_space_min.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), sp_living_space_min.getText()+"  "+position, Toast.LENGTH_SHORT).show();

            }
        });


        sp_living_space_max.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), sp_living_space_max.getText()+"  "+position, Toast.LENGTH_SHORT).show();

            }
        });

    }



    @OnClick({R.id.rdRent, R.id.rdBuy,R.id.rdPg})
    public void onRadioButtonClicked(RadioButton radioButton) {
        // Is the button now checked?
        boolean checked = radioButton.isChecked();

        // Check which radio button was clicked
        switch (radioButton.getId()) {
            case R.id.rdRent:
                if (checked) {
                    Toast.makeText(getApplicationContext(), 1+"", Toast.LENGTH_SHORT).show();

                }
                break;
            case R.id.rdBuy:
                if (checked) {
                    Toast.makeText(getApplicationContext(), 2+"", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rdPg:
                if (checked) {
                    Toast.makeText(getApplicationContext(), 3+"", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @OnClick(R.id.btn_custom_search)
    public void onButtonClick(View view) {

        Dialog dialog = new Dialog(House.this, android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_search_house);
        //dialog.getWindow().setLayout(600, 400); //Controlling width and height.
       // dialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
        dialog.getWindow().setLayout(width, height);
        dialog.show();
    }

    private void initials() {
        ((MyApplication) getApplication()).getNetComponent().inject(this);
        img_category_banner = findViewById(R.id.img_category_banner);
        storeTV = findViewById(R.id.suggest_locations);
        sp_distance = findViewById(R.id.sp_distance);
        sp_property_type = findViewById(R.id.sp_property_type);
        sp_chf_min = findViewById(R.id.sp_chf_min);
        sp_chf_max = findViewById(R.id.sp_chf_max);
        sp_room_min = findViewById(R.id.sp_room_min);
        sp_room_max = findViewById(R.id.sp_room_max);
        sp_living_space_min = findViewById(R.id.sp_living_space_min);
        sp_living_space_max = findViewById(R.id.sp_living_space_max);
        sp_sub_categories = findViewById(R.id.sp_sub_categories);

        tv_current_location.setText(getString(R.string.current_location) + "" + HomeCategoriesGrid.current_location);
        Intent i = getIntent();
        category_id = i.getIntExtra("category_id", 0);
        sub_category_id = i.getIntExtra("sub_category_id", 0);
        subcategory_inner_id = i.getIntExtra("sub_category_inner_id", 0);
        sub_category_name = i.getStringExtra("sub_category_name");
        String category_banner = i.getStringExtra("category_banner");
        sub_categories = i.getParcelableArrayListExtra("sub_categories_list");
        setSpinnerAdapter(sub_categories);
        location = HomeCategoriesGrid.current_location;
        Glide.with(getApplicationContext())
                .load(category_banner).transition(DrawableTransitionOptions.withCrossFade())
                .apply(new RequestOptions().override(600, 200))
                .into(img_category_banner);


        RemoteData remoteData = new RemoteData(this);
        remoteData.getStoreData(retrofit);
    }

    private void setSpinnerAdapter(List<MSubCategories> sub_categories) {

        sub_categories_values = new ArrayList<>();
        sub_categories_ids = new ArrayList<>();

        for (int i = 0; i < sub_categories.size(); i++) {

            sub_categories_values.add(sub_categories.get(i).getSubCatName());

        }
        for (int i = 0; i < sub_categories.size(); i++) {

            sub_categories_ids.add(sub_categories.get(i).getId());

        }


        final ArrayAdapter<String> vennue = new ArrayAdapter<String>(this, R.layout.spinner_item, sub_categories_values) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == sub_categories_values.size()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }

        };





        vennue.setDropDownViewResource(R.layout.spinner_item);
        vennue.add("Any");
        sub_categories_ids.add(0);
        sp_sub_categories.setAdapter(vennue);
        sp_sub_categories.setSelection(vennue.getCount()); //set the hint the default selection so it appears on launch.


    }

    private void setSpinnerAdapters() {

        String[] array_property_type = getResources().getStringArray(R.array.arr_property_type);
        String[] arr_chf_max = getResources().getStringArray(R.array.arr_chf_max);
        String[] arr_chf_min = getResources().getStringArray(R.array.arr_chf_min);
        String[] arr_rooms_min = getResources().getStringArray(R.array.arr_rooms_min);
        String[] arr_rooms_max = getResources().getStringArray(R.array.arr_rooms_max);
        String[] arr_living_space_min = getResources().getStringArray(R.array.arr_living_space_min);
        String[] arr_living_space_max = getResources().getStringArray(R.array.arr_living_space_max);


        ArrayAdapter<String> adapterPropertType = new ArrayAdapter<String>(this,
                R.layout.spinner_item, array_property_type);
        sp_property_type.setAdapter(adapterPropertType);

        ArrayAdapter<String> adapterChfMax = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arr_chf_max);
        sp_chf_max.setAdapter(adapterChfMax);

        ArrayAdapter<String> adapterChfMin = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arr_chf_min);
        sp_chf_min.setAdapter(adapterChfMin);

        ArrayAdapter<String> adapterRoomMin = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arr_rooms_min);
        sp_room_min.setAdapter(adapterRoomMin);

        ArrayAdapter<String> adapterRoomMax = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arr_rooms_max);
        sp_room_max.setAdapter(adapterRoomMax);

        ArrayAdapter<String> adapterLivingSpaceMin = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arr_living_space_min);
        sp_living_space_min.setAdapter(adapterLivingSpaceMin);

        ArrayAdapter<String> adapterLivingSpaceMax = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arr_living_space_max);
        sp_living_space_max.setAdapter(adapterLivingSpaceMax);
    }

}
