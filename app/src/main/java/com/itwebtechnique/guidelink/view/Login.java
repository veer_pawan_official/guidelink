package com.itwebtechnique.guidelink.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.Utils.AlertUtils;
import com.itwebtechnique.guidelink.app.MyApplication;
import com.itwebtechnique.guidelink.databinding.ActivityLoginBinding;
import com.itwebtechnique.guidelink.model.SuccessAllCategories;
import com.itwebtechnique.guidelink.viewmodel.LoginViewModel;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Retrofit;

public class Login extends BaseActivity {

    @Inject
    Retrofit retrofit;

    public AwesomeValidation awesomeValidation;
    @BindView(R.id.tv_register)
    TextView tv_register;
    @BindView(R.id.tv_forget_password)
    TextView tv_forget_password;
    @BindView(R.id.et_login_email)
    EditText et_login_email;
    @BindView(R.id.et_login_password)
    EditText et_login_password;
    SuccessAllCategories return_success_AllCategories_message;
    @BindView(R.id.rd_remember_me)
    RadioButton rd_remember_me;


    private static final String LOCALE_KEY = "localekey";
    private static final String HINDI_LOCALE = "hi";
    private static final String ENGLISH_LOCALE = "en_US";
    private static final String LOCALE_PREF_KEY = "localePref";
    private Locale locale;


    private LoginViewModel loginViewModel;
    private ActivityLoginBinding activityLoginBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding= DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginViewModel = new LoginViewModel(this);
        activityLoginBinding.setLoginview(loginViewModel);
        ButterKnife.bind(this);
        getCurrentLocation();
        initials();




        activityLoginBinding.setPresenter(() -> {

            if (awesomeValidation.validate()) {
                if (validate()) {
                    String email = et_login_email.getText().toString().trim();
                    String password = et_login_password.getText().toString().trim();


                    loginViewModel.sendLoginRequest(email, password,retrofit);
                }
            }
        });

        tv_register.setOnClickListener(v -> {


            if (isNetworkAvailable()) {


               // requestStoragePermission();

                Intent i = new Intent(getApplicationContext(), Registration.class);

                startActivity(i);
            }
        });
        tv_forget_password.setOnClickListener(v -> {
            if (isNetworkAvailable()) {
                Intent i = new Intent(getApplicationContext(), ForgetPassword.class);

                startActivity(i);
            }
        });
    }

    private void getCurrentLocation() {

        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = false;
        if (service != null) {
            enabled = service
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
    }

    public void initials() {
        ((MyApplication) getApplication()).getNetComponent().inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_header);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.et_login_email, Patterns.EMAIL_ADDRESS, R.string.emailerror);


    }

   /* @OnClick(R.id.btn_signin)
    public void onButtonClick(View view) {

        if (isNetworkAvailable()) {
            if (awesomeValidation.validate()) {
                if (validate()) {
                    String email = et_login_email.getText().toString().trim();
                    String password = et_login_password.getText().toString().trim();
                    loginUser(email, password);
                }
            }
        }

    }*/

/*    private void loginUser(String email, String password) {
        showProgressDialog();
        Api apiService = ApiClient.getClient().create(Api.class);
        Call<SuccessAllCategories> call = apiService.user_login(email, password);
        call.enqueue(new Callback<SuccessAllCategories>() {
            @Override
            public void onResponse(@NonNull Call<SuccessAllCategories> call, @NonNull Response<SuccessAllCategories> response) {
                hideProgressDialog();
                return_success_AllCategories_message = response.body();
                if (response.code() == 200) {
                    if (return_success_AllCategories_message != null) {
                        if (return_success_AllCategories_message.getSuccess() == 1) {
                            Intent i = new Intent(getApplicationContext(), HomeCategoriesGrid.class);
                            startActivity(i);
                            finish();
                        } else {
                            AlertUtils.showToast(Login.this, return_success_AllCategories_message.getMsg());
                        }
                    } else {
                        AlertUtils.showToast(Login.this, response.message());
                    }
                } else {
                    AlertUtils.showToast(Login.this, response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SuccessAllCategories> call, Throwable t) {
                AlertUtils.showToast(Login.this, return_success_AllCategories_message.getMsg());
                hideProgressDialog();
            }
        });
    }*/

    private boolean validate() {
        boolean temp = true;
        if (!rd_remember_me.isChecked()) {
            AlertUtils.showToast(Login.this, "Please agree terms and condition!!");
            temp = false;
        }
        return temp;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeCategoriesGrid/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_language) {
//            Modifying Locale if User clicked language from options pane
            Resources resources = getResources();
            SharedPreferences sharedPreferences = getSharedPreferences("localePref", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            if(sharedPreferences.getString(LOCALE_KEY, ENGLISH_LOCALE).equals(HINDI_LOCALE)){
                locale = new Locale(ENGLISH_LOCALE);
                editor.putString(LOCALE_KEY, ENGLISH_LOCALE);
            } else {
                locale = new Locale(HINDI_LOCALE);
                editor.putString(LOCALE_KEY, HINDI_LOCALE);
            }
            //editor.commit();
            editor.apply();

            Configuration configuration = resources.getConfiguration();
            configuration.setLocale(locale);
            getBaseContext().getResources().updateConfiguration(configuration,
                    getBaseContext().getResources().getDisplayMetrics());
            recreate();
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        SharedPreferences sharedPreferences = getSharedPreferences("localePref", MODE_PRIVATE);

        MenuItem item = menu.getItem(0);
        if(sharedPreferences.getString(LOCALE_KEY, ENGLISH_LOCALE).equals(HINDI_LOCALE)){
            item.setTitle("English");
        } else {
            item.setTitle("German");

        }

        return true;
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

}
