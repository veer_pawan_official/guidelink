package com.itwebtechnique.guidelink.view;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.Utils.AlertUtils;
import com.itwebtechnique.guidelink.Utils.Utils;


@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {





    private RelativeLayout rlProgress;
    private View mainLayout;
    private ProgressDialog mProgressDialog;

    @Override
    public void setContentView(View view) {
        super.setContentView(view);

    }


    protected boolean isNetworkAvailable() {
        if (Utils.isNetworkAvailable(this)) {
            return true;
        } else {
            AlertUtils.showToast(this, getString(R.string.no_internet_connection));
            return false;
        }
    }

    public void showProgressDialog() {
        showProgressDialog(getString(R.string.progress_message));
    }

    protected void showProgressDialog(String message) {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
            }
            mProgressDialog.setMessage(message);
            mProgressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * it is used to set visible status of main layout and progress layout
     *
     * @param mainViewStatus
     * @param progressStatus
     */
    public void setVisibilityOfProgress(int mainViewStatus, int progressStatus) {
        if (mainLayout != null && rlProgress != null) {
            mainLayout.setVisibility(mainViewStatus);
            rlProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            setVisibilityOfProgress(progressStatus);
        }
    }

    /**
     * it is used to set visibility status of progress layout..
     *
     * @param visibleStatus
     * @return
     */
    private void setVisibilityOfProgress(int visibleStatus) {
        if (rlProgress != null)
            rlProgress.setVisibility(visibleStatus);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
