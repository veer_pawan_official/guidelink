package com.itwebtechnique.guidelink.view;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.Utils.AlertUtils;
import com.itwebtechnique.guidelink.model.ProductDetailOfSubCategory;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductDescription extends BaseActivity implements OnMapReadyCallback {

    SupportMapFragment mapFragment;
    @BindView(R.id.product_description)
    TextView product_description;
    @BindView(R.id.img_product)
    ImageView img_product;
    @BindView(R.id.tv_fax_number)
    TextView tv_fax_number;
    @BindView(R.id.tv_mobile_number)
    TextView tv_mobile_number;
    @BindView(R.id.tv_email_address)
    TextView tv_email_address;
    @BindView(R.id.tv_website)
    TextView tv_website;
    @BindView(R.id.tv_address)
    TextView tv_address;
    int category_id;
    double latitude, longitude;
    String mobileNumber, faxNumber, emailId, websiteUrl, address, location, latitute, longnitude, logo, shopName, fullName;
    int postId;
    //List<ProductDetailOfSubCategory> subCategories;
    private int post_id;
    /*@BindView(R.id.img_share_fb)
    ImageView img_share_fb;

    @BindView(R.id.img_share_twitter)
    ImageView img_share_twitter;

    @BindView(R.id.img_share_pinterest)
    ImageView img_share_pinterest;

    @BindView(R.id.img_share_linkedin)
    ImageView img_share_linkedin;

    @BindView(R.id.img_share_google)
    ImageView img_share_google;

    @BindView(R.id.img_share_whatsapp)
    ImageView img_share_whatsapp;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_description);
        ButterKnife.bind(this);


        Toolbar toolbar = (Toolbar) findViewById(R.id.main_header);
        setSupportActionBar(toolbar);

        initials();

       /* img_share_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String message = "https://guidelink.ch/demo/marketing/businesspost-details"+"/"+category_id+"/"+post_id;
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);

                startActivity(Intent.createChooser(share, "Title of the dialog the system will open"));

               *//* Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Content to share");
                PackageManager pm = v.getContext().getPackageManager();
                List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
                for (final ResolveInfo app : activityList) {
                    if ((app.activityInfo.name).contains("facebook")) {
                        final ActivityInfo activity = app.activityInfo;
                        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |             Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                        shareIntent.setComponent(name);
                        v.getContext().startActivity(shareIntent);
                        break;
                    }
                }*//*
            }
        });

        img_share_twitter.setOnClickListener(v -> {


            String url = "http://www.twitter.com/intent/tweet?url=YOURURL&text=https://guidelink.ch/demo/marketing/businesspost-details"+"/"+category_id+"/"+post_id;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);

        });
        img_share_whatsapp.setOnClickListener(v -> {
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("text/plain");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, "The text you wanted to share");
            try {
                startActivity(whatsappIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                AlertUtils.showToast(getApplicationContext(),"App not install");
            }
        });*/

    }

    private void initials() {
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Intent i = getIntent();
        int position = i.getIntExtra("position", 0);
        //subCategories = i.getParcelableArrayListExtra("arr_subcategories");
        category_id = i.getIntExtra("category_id", 0);
        postId = i.getIntExtra("post_id", 0);
        // Toast.makeText(getApplicationContext(),category_id+"",Toast.LENGTH_LONG).show();

        mobileNumber = i.getStringExtra("mobile_number");
        faxNumber = i.getStringExtra("fax_number");
        emailId = i.getStringExtra("email_id");
        websiteUrl = i.getStringExtra("website_url");
        address = i.getStringExtra("address");
        location = i.getStringExtra("location");
        latitute = i.getStringExtra("latitude");
        longnitude = i.getStringExtra("longitude");
        logo = i.getStringExtra("logo");
        shopName = i.getStringExtra("shop_name");
        fullName = i.getStringExtra("full_name");


        Picasso.with(getApplicationContext())
                .load(logo)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(img_product);

      /*  Glide.with(getApplicationContext())
                .load(subCategories.get(position).getLogo()).transition(DrawableTransitionOptions.withCrossFade())
                .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder))
                .into(img_product);*/
        tv_fax_number.setText(String.format("%s", faxNumber));
        tv_mobile_number.setText(String.format("%s", mobileNumber));
        tv_email_address.setText(String.format("%s", emailId));
        tv_website.setText(String.format("%s", websiteUrl));
        tv_address.setText(String.format("%s", address + " " +System.lineSeparator()+ location));
        post_id = postId;

        latitude = Double.parseDouble(latitute);
        longitude = Double.parseDouble(longnitude);
        if (category_id == 5) {
            product_description.setText(fullName);
        } else {
            product_description.setText(String.format("%s", "Description of : " + shopName));

        }

        img_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // open the desired page
                openWebPage(websiteUrl);
              /*  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(subCategories.get(position).getWebsiteURL()));

                startActivity(browserIntent);*/

            }
        });


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        setgooglemaplocation(latitude, longitude);
    }

    private void setgooglemaplocation(double latitude, double longitude) {

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                //googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

               /* googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(37.4233438, -122.0728817))
                        .title("LinkedIn")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(37.4629101,-122.2449094))
                        .title("Facebook")
                        .snippet("Facebook HQ: Menlo Park"));
*/
                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .title("Place"));

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 10));
            }
        });
    }

    public void openWebPage(String url) {

        Uri webpage = Uri.parse(url);

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            webpage = Uri.parse("http://" + url);
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4233438, -122.0728817))
                .title("LinkedIn")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4629101, -122.2449094))
                .title("Facebook")
                .snippet("Facebook HQ: Menlo Park"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.3092293, -122.1136845))
                .title("Apple"));

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(37.4233438, -122.0728817), 10));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

}
