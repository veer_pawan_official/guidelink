package com.itwebtechnique.guidelink.view;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.Utils.AlertUtils;
import com.itwebtechnique.guidelink.Utils.RecyclerItemClickListener;
import com.itwebtechnique.guidelink.adapter.SubCategoriesAdapter;
import com.itwebtechnique.guidelink.app.MyApplication;
import com.itwebtechnique.guidelink.model.MAllCategories;
import com.itwebtechnique.guidelink.model.MSubCategories;
import com.itwebtechnique.guidelink.model.SuccessSubCategories;
import com.itwebtechnique.guidelink.network.Api;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class SubCategory extends BaseActivity {

    public static boolean isViewWithCatalog = false;
    @Inject
    Retrofit retrofit;
    ImageView img_category_banner;
    SuccessSubCategories successAllCategories;
    List<MAllCategories> categories;
    List<MSubCategories> sub_categories;
    private RecyclerView recyclerView;
    private SubCategoriesAdapter mAdapter;
    private int category_id;
    private TextView errorText;
    private ProgressBar loader;
    private String category_banner;
    private int sub_category_id, sub_category_inner_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        ButterKnife.bind(this);
        initials();
        getSubCategories(category_id);


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), (view, position) -> {


                    sub_category_id = sub_categories.get(position).getId();
                    sub_category_inner_id = sub_categories.get(position).getIsInnerSubCategoryAvailbale();

                    //Toast.makeText(getApplicationContext(),category_id+"",Toast.LENGTH_LONG).show();


                    if (category_id == 2) {

                        Intent i1 = new Intent(getApplicationContext(), House.class);
                        i1.putExtra("category_id", category_id);
                        i1.putExtra("sub_category_id", sub_category_id);
                        i1.putExtra("sub_category_inner_id", sub_category_inner_id);
                        i1.putExtra("sub_category_name", sub_categories.get(position).getSubCatName());
                        i1.putExtra("sub_categories_list", (ArrayList) sub_categories);
                        i1.putExtra("category_banner", category_banner);
                        i1.putExtra("sub_category", "1");
                        startActivity(i1);
                    } else if (category_id == 16) {


                        Intent i1 = new Intent(getApplicationContext(), Cars.class);
                        i1.putExtra("category_id", category_id);
                        i1.putExtra("sub_category_id", sub_category_id);
                        i1.putExtra("sub_category_inner_id", sub_category_inner_id);
                        i1.putExtra("sub_category_name", sub_categories.get(position).getSubCatName());
                        i1.putExtra("sub_categories_list", (ArrayList) sub_categories);
                        i1.putExtra("category_banner", category_banner);
                        i1.putExtra("sub_category", "1");
                        startActivity(i1);
                    } else {


                        Intent i1 = new Intent(getApplicationContext(), CatSubcategoriesProducts.class);
                        i1.putExtra("category_id", category_id);
                        i1.putExtra("sub_category_id", sub_category_id);
                        i1.putExtra("sub_category_inner_id", sub_category_inner_id);
                        i1.putExtra("sub_category_name", sub_categories.get(position).getSubCatName());
                        i1.putExtra("sub_categories_list", (ArrayList) sub_categories);
                        i1.putExtra("category_banner", category_banner);
                        i1.putExtra("sub_category", "1");
                        startActivity(i1);
                    }


                })
        );


    }

   /* @OnClick(R.id.btn_grid)
    public void onButtonClick1(View view) {
        isViewWithCatalog = !isViewWithCatalog;


        //loading = false;
        recyclerView.setLayoutManager(isViewWithCatalog ? new GridLayoutManager(getApplicationContext(), 3):  new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(mAdapter);

    }*/


    public void initials() {

        ((MyApplication) getApplication()).getNetComponent().inject(this);
        Toolbar toolbar = findViewById(R.id.main_header);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        errorText = findViewById(R.id.error_text);
        loader = findViewById(R.id.feed_loading);
        recyclerView = findViewById(R.id.recyclerview_subcategory);
        img_category_banner = findViewById(R.id.img_category_banner);

        Intent i = getIntent();
        category_id = i.getIntExtra("category_id", 0);
        //Toast.makeText(getApplicationContext(),category_id+"",Toast.LENGTH_LONG).show();
        categories = i.getParcelableArrayListExtra("categories_list");
        category_banner = i.getStringExtra("category_banner");
        Glide.with(getApplicationContext())
                .load(category_banner).transition(DrawableTransitionOptions.withCrossFade())
                .apply(new RequestOptions().override(600, 200))
                .into(img_category_banner);


    }

    private void getSubCategories(int category_id) {
        loader.setVisibility(VISIBLE);
        Api api = retrofit.create(Api.class);
        Call<SuccessSubCategories> call = api.getSubCategories(category_id);
        call.enqueue(new Callback<SuccessSubCategories>() {
            @Override
            public void onResponse(@NonNull Call<SuccessSubCategories> call, @NonNull Response<SuccessSubCategories> response) {
                hideProgressDialog();
                successAllCategories = response.body();
                loader.setVisibility(GONE);
                errorText.setVisibility(GONE);
                if (response.code() == 200) {
                    if (successAllCategories != null) {

                        if (successAllCategories.getSuccess() == 1) {
                            sub_categories = response.body().getSubcategories();
                            if (sub_categories != null) {
                                setAdapterView(sub_categories);
                            }

                        } else {
                            AlertUtils.showToast(SubCategory.this, successAllCategories.getMsg());
                        }
                    } else {
                        AlertUtils.showToast(SubCategory.this, response.message());
                    }
                } else {
                    AlertUtils.showToast(SubCategory.this, response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SuccessSubCategories> call, @NonNull Throwable t) {
                AlertUtils.showToast(SubCategory.this, t.getMessage());

                loader.setVisibility(GONE);
                errorText.setVisibility(VISIBLE);
                errorText.setText(R.string.error_string);
            }
        });
    }

    private void setAdapterView(List<MSubCategories> categories) {
        // isViewWithCatalog = true;
        mAdapter = new SubCategoriesAdapter(getApplicationContext(), categories);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

}
