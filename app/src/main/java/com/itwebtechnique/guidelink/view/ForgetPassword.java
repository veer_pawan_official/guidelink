package com.itwebtechnique.guidelink.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.Utils.AlertUtils;
import com.itwebtechnique.guidelink.app.MyApplication;
import com.itwebtechnique.guidelink.model.SuccessAllCategories;
import com.itwebtechnique.guidelink.network.Api;
import com.itwebtechnique.guidelink.viewmodel.successViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForgetPassword extends BaseActivity {
    @Inject
    Retrofit retrofit;

    @BindView(R.id.et_email)
    EditText et_email;
    AwesomeValidation awesomeValidation;
    successViewModel model;
    Button btn_send;

    SuccessAllCategories return_success_AllCategories_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        initials();

        btn_send.setOnClickListener(v -> {
            if (awesomeValidation.validate()) {
                if (isNetworkAvailable()) {
                  showProgressDialog();
                    String email_address = et_email.getText().toString().trim();
                    loadData(email_address);
                }
            }
        });


    }

    public void initials() {
        ((MyApplication) getApplication()).getNetComponent().inject(this);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.et_email, Patterns.EMAIL_ADDRESS, R.string.emailerror);
        btn_send = findViewById(R.id.btn_send);
        model = ViewModelProviders.of(this).get(successViewModel.class);

    }


    private void loadData(String email_address) {
        Api api = retrofit.create(Api.class);
        Call<SuccessAllCategories> call = api.sendPasswordLing(email_address);

        call.enqueue(new Callback<SuccessAllCategories>() {
            @Override
            public void onResponse(@NonNull Call<SuccessAllCategories> call, @NonNull Response<SuccessAllCategories> response) {
               hideProgressDialog();

                return_success_AllCategories_message = response.body();
                if (return_success_AllCategories_message != null) {
                    AlertUtils.showToast(ForgetPassword.this, return_success_AllCategories_message.getMsg());
                }else
                {
                    AlertUtils.showToast(ForgetPassword.this, response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SuccessAllCategories> call, Throwable t) {
                AlertUtils.showToast(ForgetPassword.this, return_success_AllCategories_message.getMsg());
               hideProgressDialog();

            }
        });
    }


}
