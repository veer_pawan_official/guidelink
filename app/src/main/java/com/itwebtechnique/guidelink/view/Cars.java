package com.itwebtechnique.guidelink.view;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.app.MyApplication;
import com.itwebtechnique.guidelink.fragments.ViewPagerAdapter;
import com.itwebtechnique.guidelink.model.MSubCategories;
import com.itwebtechnique.guidelink.viewmodel.RemoteData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class Cars extends BaseActivity implements TabLayout.OnTabSelectedListener {
    @Inject
    Retrofit retrofit;

    Spinner sp_sub_categories;
    Spinner sp_distance;
    ImageView img_category_banner;
    List<MSubCategories> sub_categories;
    ArrayList<String> sub_categories_values = new ArrayList<>();
    ArrayList<Integer> sub_categories_ids = new ArrayList<Integer>();
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    private String sub_category_name;
    private String distance_in_km = "5";
    private String location = HomeCategoriesGrid.current_location;
    private int category_id;
    private TextView errorText;
    private ProgressBar loader;
    private int sub_category_id, subcategory_inner_id, inner_sub_categories_id;
    private AutoCompleteTextView storeTV;
    private int[] tabIcons = {
            R.drawable.car,
            R.drawable.commercial_vehicle,
            R.drawable.truck,
            R.drawable.mobile_home_car,
            R.drawable.pendant,
            R.drawable.motorcycle
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cars);
        initials();
        /*storeTV.setOnItemClickListener((parent, view, position, id) -> {

            location = parent.getItemAtPosition(position).toString();

            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        });*/
       /* sp_distance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                distance_in_km = item.replaceAll("[^0-9]", "");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_sub_categories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sub_category_id = sub_categories_ids.get(position);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
*/
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabGravity(Gravity.FILL_HORIZONTAL);

        setupTabIcons();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);
        tabLayout.getTabAt(5).setIcon(tabIcons[5]);
    }

    private void initials() {
        ((MyApplication) getApplication()).getNetComponent().inject(this);
        img_category_banner = findViewById(R.id.img_category_banner);
        storeTV = findViewById(R.id.suggest_locations);
        sp_distance = findViewById(R.id.sp_distance);
        sp_sub_categories = findViewById(R.id.sp_sub_categories);
        Intent i = getIntent();

        category_id = i.getIntExtra("category_id", 0);
        sub_category_id = i.getIntExtra("sub_category_id", 0);
        subcategory_inner_id = i.getIntExtra("sub_category_inner_id", 0);
        sub_category_name = i.getStringExtra("sub_category_name");
        String category_banner = i.getStringExtra("category_banner");

        sub_categories = i.getParcelableArrayListExtra("sub_categories_list");
        //setSpinnerAdapter(sub_categories);
        location = HomeCategoriesGrid.current_location;
        Glide.with(getApplicationContext())
                .load(category_banner).transition(DrawableTransitionOptions.withCrossFade())
                .apply(new RequestOptions().override(600, 200))
                .into(img_category_banner);

      /*  RemoteData remoteData = new RemoteData(this);
        remoteData.getStoreData(retrofit);*/


    }

    private void setSpinnerAdapter(List<MSubCategories> sub_categories) {

        sub_categories_values = new ArrayList<>();
        sub_categories_ids = new ArrayList<>();

        for (int i = 0; i < sub_categories.size(); i++) {

            sub_categories_values.add(sub_categories.get(i).getSubCatName());

        }
        for (int i = 0; i < sub_categories.size(); i++) {

            sub_categories_ids.add(sub_categories.get(i).getId());

        }


        final ArrayAdapter<String> vennue = new ArrayAdapter<String>(this, R.layout.spinner_item, sub_categories_values) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == sub_categories_values.size()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }

        };


       /* vennue.setDropDownViewResource(R.layout.spinner_item);*/
        vennue.setDropDownViewResource(R.layout.spinner_item);

        vennue.add(sub_category_name);
        sub_categories_ids.add(sub_category_id);


        sp_sub_categories.setAdapter(vennue);
        sp_sub_categories.setSelection(vennue.getCount()); //set the hint the default selection so it appears on launch.


    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());


    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

}
