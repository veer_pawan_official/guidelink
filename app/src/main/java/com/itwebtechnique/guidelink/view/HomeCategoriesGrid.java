package com.itwebtechnique.guidelink.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.Utils.AlertUtils;
import com.itwebtechnique.guidelink.Utils.RecyclerItemClickListener;
import com.itwebtechnique.guidelink.adapter.MainCategoriesAdapter;
import com.itwebtechnique.guidelink.adapter.SliderPagerAdapter;
import com.itwebtechnique.guidelink.app.MyApplication;
import com.itwebtechnique.guidelink.map.GetAddressIntentService;
import com.itwebtechnique.guidelink.model.MAllCategories;
import com.itwebtechnique.guidelink.model.MSubCategories;
import com.itwebtechnique.guidelink.model.SuccessAllCategories;
import com.itwebtechnique.guidelink.model.SuccessSubCategories;
import com.itwebtechnique.guidelink.network.Api;


import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class HomeCategoriesGrid extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2;
    public static String current_location;
    @Inject
    Retrofit retrofit;
    SuccessAllCategories return_success_AllCategories_message;
    List<MAllCategories> categories = new ArrayList<>();
    SliderPagerAdapter sliderPagerAdapter;
    ArrayList<Integer> slider_image_list;
    int page_position = 0;
    EditText edt_search;
    String manufacturer = "xiaomi";
    SuccessSubCategories successAllCategories;
    List<MSubCategories> sub_categories;
    @BindView(R.id.btn_retry)
    Button btn_retry;
    private RecyclerView recyclerView;
    private MainCategoriesAdapter mAdapter;
    private ViewPager vp_slider;
    private LinearLayout ll_dots;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationAddressResultReceiver addressResultReceiver;
    private Location currentLocation;
    private LocationCallback locationCallback;
    private int category_id, isSubcategoryAwailable;
    private String category_banner;
    private ProgressBar loader;
    private TextView errorText;
    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        addressResultReceiver = new LocationAddressResultReceiver(new Handler());
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                currentLocation = locationResult.getLocations().get(0);
                getAddress();
            }
        };

        startLocationUpdates();
        initials();
        getAllcategories();
        initViewPager();
        cicleIndicator();
      /*  edt_search.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), GoogleSearchCustom.class);

            startActivity(i);
        });*/


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), (view, position) -> {

                    category_id = categories.get(position).getId();
                    category_banner = categories.get(position).getBanner();
                    isSubcategoryAwailable = categories.get(position).getIsSubCategoryAvailable();
                    if (categories.get(position).getIsSubCategoryAvailable() == 1) {


                        if (category_id == 10 ||category_id == 28 || category_id == 18 || category_id == 19 ||  category_id == 25 || category_id == 7 || category_id == 23 || category_id == 27 || category_id == 21 ||
                               /* category_id == 16 || */category_id == 3 || category_id == 14) {
                            AlertUtils.showToast(getApplicationContext(), "Work in progress");

                        } else {

                            getSubCategories(category_id, isSubcategoryAwailable);


                        }


                    } else {


                        if (category_id == 10 ||category_id == 28 || category_id == 18 || category_id == 19 ||  category_id == 25 || category_id == 7 || category_id == 23 || category_id == 27 ||  category_id == 21 ||
                              /*  category_id == 16 ||*/ category_id == 3 || category_id == 14) {
                            AlertUtils.showToast(getApplicationContext(), "Work in progress");
                        } else {


                            Intent i1 = new Intent(getApplicationContext(), CatSubcategoriesProducts.class);
                            i1.putExtra("category_id", category_id);
                            i1.putExtra("categories_list", (ArrayList) categories);
                            i1.putExtra("category_banner", category_banner);
                            i1.putExtra("sub_category", "0");
                            startActivity(i1);

                        }


                    }

                })
        );

    }


    @SuppressWarnings("MissingPermission")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        } else {


            @SuppressLint("RestrictedApi")
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(10000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            //locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }
    }

    @SuppressWarnings("MissingPermission")
    private void getAddress() {
        if (!Geocoder.isPresent()) {
            Toast.makeText(HomeCategoriesGrid.this,
                    "Can't find current address,",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, GetAddressIntentService.class);
        intent.putExtra("add_receiver", addressResultReceiver);
        intent.putExtra("add_location", currentLocation);
        startService(intent);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                } else {

                    if (manufacturer.equalsIgnoreCase(android.os.Build.MANUFACTURER)) {
                        //this will open auto start screen where user can enable permission for your app
                        Intent intent1 = new Intent();
                        intent1.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
                        startActivity(intent1);
                    }


                    AlertUtils.showToast(getApplicationContext(), "Location permission not granted," +
                            "restart the app if you want the feature");
                   /* Toast.makeText(this, "Location permission not granted," +
                                    "restart the app if you want the feature",
                            Toast.LENGTH_SHORT).show();*/
                }
                return;
            }
        }
    }

  /*  private void showResults(String currentAdd, String locality, String picode) {

        //  Toast.makeText(getApplicationContext(),locality+"     "+picode,Toast.LENGTH_LONG).show();


    }
*/

    @OnClick(R.id.btnSwitchView)
    public void onButtonClick(View view) {

        if (isNetworkAvailable()) {
            Intent i = new Intent(getApplicationContext(), HomeCategoriesList.class);
            i.putExtra("categories_list", (ArrayList) categories);
            startActivity(i);
        }

    }

    @OnClick(R.id.btn_loginSignUp)
    public void onButtonClick1(View view) {

        if (isNetworkAvailable()) {
            Intent i = new Intent(getApplicationContext(), Login.class);

            startActivity(i);
        }

    }


    @OnClick(R.id.btn_retry)
    public void onRetryButtonClick1(View view) {

        if (isNetworkAvailable()) {

            getAllcategories();
        }

    }

    private void cicleIndicator() {
        addBottomDots(0);

        final Handler handler = new Handler();

        final Runnable update = () -> {
            if (page_position == slider_image_list.size()) {
                page_position = 0;
            } else {
                page_position = page_position + 1;
            }
            vp_slider.setCurrentItem(page_position, true);
        };

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 100, 2000);
    }

    private void initials() {
        ((MyApplication) getApplication()).getNetComponent().inject(this);
        recyclerView = findViewById(R.id.recyclerview);
        edt_search = findViewById(R.id.edt_search);
        errorText = findViewById(R.id.error_text);
        loader = findViewById(R.id.feed_loading);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.subject_swipe_refresh_layout);
        swipeContainer.setOnRefreshListener(this);
        swipeContainer.setRefreshing(false);


    }


    public void getAllcategories() {

        loader.setVisibility(VISIBLE);
        Api api = retrofit.create(Api.class);
        Call<SuccessAllCategories> call = api.getAllCategories();
        call.enqueue(new Callback<SuccessAllCategories>() {
            @Override
            public void onResponse(@NonNull Call<SuccessAllCategories> call, @NonNull Response<SuccessAllCategories> response) {
                loader.setVisibility(GONE);
                errorText.setVisibility(GONE);
                btn_retry.setVisibility(View.GONE);
                return_success_AllCategories_message = response.body();


                if (response.code() == 200) {
                    //mAdapter.notifyDataSetChanged();
                    if (return_success_AllCategories_message != null) {
                        if (response.body() != null) {
                            categories = response.body().getMaincategoriesData().getInfoData();
                            if (return_success_AllCategories_message.getSuccess() == 1) {
                                setAdapterView();
                            } else {
                                AlertUtils.showToast(HomeCategoriesGrid.this, response.message());
                            }
                        } else {
                            AlertUtils.showToast(HomeCategoriesGrid.this, response.message());
                        }
                    } else {
                        AlertUtils.showToast(HomeCategoriesGrid.this, response.message());
                    }
                } else {
                    AlertUtils.showToast(HomeCategoriesGrid.this, response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SuccessAllCategories> call, @NonNull Throwable t) {
                // AlertUtils.showToast(HomeCategoriesGrid.this, return_success_AllCategories_message.getMsg());
                // swipeContainer.setRefreshing(true);
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();

                loader.setVisibility(GONE);
                errorText.setVisibility(VISIBLE);
                errorText.setText(R.string.error_string);
                btn_retry.setVisibility(View.VISIBLE);
                // mAdapter.notifyItemRangeRemoved(0, categories.size());


            }
        });

    }

    private void setAdapterView() {
        mAdapter = new MainCategoriesAdapter(HomeCategoriesGrid.this, categories);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }

    private void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[slider_image_list.size()];

        ll_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#000000"));
            ll_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
    }

    private void initViewPager() {


        vp_slider = findViewById(R.id.vp_slider);
        ll_dots = findViewById(R.id.ll_dots);
        slider_image_list = new ArrayList<>();


        slider_image_list.add(R.drawable.banner2);
        slider_image_list.add(R.drawable.banner3);
       /* slider_image_list.add("https://i1.wp.com/aarsi.in/wp-content/uploads/2016/12/flipkart-same-day-delivery-1.jpg?resize=480%2C250");
        slider_image_list.add("https://previews.123rf.com/images/alhovik/alhovik1605/alhovik160500067/59667793-wow-mega-sale-banni%C3%A8re-offre-sp%C3%A9ciale-grande-vente-super-plat-60-de-r%C3%A9duction-.jpg");
        slider_image_list.add("https://i.ytimg.com/vi/2rcGae6yTeA/maxresdefault.jpg");
        slider_image_list.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_NocvPJaq4hAqhHMpfyLdXPnE2_7zzzuPRtvxoKg8_aN21wZcOA");
*/

        sliderPagerAdapter = new SliderPagerAdapter(HomeCategoriesGrid.this, slider_image_list);
        vp_slider.setAdapter(sliderPagerAdapter);

        vp_slider.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        startLocationUpdates();
        //fusedLocationClient.removeLocationUpdates(locationCallback);
    }

    private void getSubCategories(int category_idd, int isSubcategoryAwailable) {
        //Toast.makeText(getApplicationContext(), category_idd + "", Toast.LENGTH_LONG).show();
        loader.setVisibility(VISIBLE);
        Api api = retrofit.create(Api.class);
        Call<SuccessSubCategories> call = api.getSubCategories(category_idd);
        call.enqueue(new Callback<SuccessSubCategories>() {
            @Override
            public void onResponse(@NonNull Call<SuccessSubCategories> call, @NonNull Response<SuccessSubCategories> response) {
                hideProgressDialog();
                successAllCategories = response.body();
                loader.setVisibility(GONE);
                errorText.setVisibility(GONE);
                if (response.code() == 200) {
                    if (successAllCategories != null) {

                        if (successAllCategories.getSuccess() == 1) {
                            sub_categories = response.body().getSubcategories();
                            if (sub_categories != null) {


                                if (category_idd == 2) {
                                    Intent i1 = new Intent(getApplicationContext(), House.class);
                                    i1.putExtra("category_id", category_id);
                                    i1.putExtra("categories_list", (ArrayList) categories);
                                    i1.putExtra("category_banner", category_banner);
                                    i1.putExtra("sub_category", isSubcategoryAwailable + "");
                                    i1.putExtra("sub_categories_list", (ArrayList) sub_categories);
                                    startActivity(i1);
                                } else if (category_idd == 16) {
                                    Intent i1 = new Intent(getApplicationContext(), Cars.class);
                                    i1.putExtra("category_id", category_id);
                                    i1.putExtra("categories_list", (ArrayList) categories);
                                    i1.putExtra("category_banner", category_banner);
                                    i1.putExtra("sub_category", isSubcategoryAwailable + "");
                                    i1.putExtra("sub_categories_list", (ArrayList) sub_categories);
                                    startActivity(i1);

                                } else {
                                    Intent i1 = new Intent(getApplicationContext(), CatSubcategoriesProducts.class);
                                    i1.putExtra("category_id", category_id);
                                    i1.putExtra("categories_list", (ArrayList) categories);
                                    i1.putExtra("category_banner", category_banner);
                                    i1.putExtra("sub_category", isSubcategoryAwailable + "");
                                    i1.putExtra("sub_categories_list", (ArrayList) sub_categories);
                                    startActivity(i1);
                                }
                            }

                        } else {
                            AlertUtils.showToast(HomeCategoriesGrid.this, successAllCategories.getMsg());
                        }
                    } else {
                        AlertUtils.showToast(HomeCategoriesGrid.this, response.message());
                    }
                } else {
                    AlertUtils.showToast(HomeCategoriesGrid.this, response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SuccessSubCategories> call, @NonNull Throwable t) {
                AlertUtils.showToast(HomeCategoriesGrid.this, t.getMessage());
                //swipeContainer.setRefreshing(false);
                loader.setVisibility(GONE);
                errorText.setVisibility(VISIBLE);
                errorText.setText(R.string.error_string);
            }
        });
    }

    @Override
    public void onRefresh() {
        getAllcategories();
        swipeContainer.setRefreshing(false);
    }

    private class LocationAddressResultReceiver extends ResultReceiver {
        LocationAddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultCode == 0) {
                //Last Location can be null for various reasons
                //for example the api is called first time
                //so retry till location is set
                //since intent service runs on background thread, it doesn't block main thread
                Log.d("Address", "Location null retrying");
                getAddress();
            }

            if (resultCode == 1) {
                Toast.makeText(HomeCategoriesGrid.this,
                        "Address not found, ",
                        Toast.LENGTH_SHORT).show();
            }

            String currentAdd = resultData.getString("address_result");
            String locality = resultData.getString("locality");
            String pincode = resultData.getString("postalCode");

            //showResults(currentAdd, locality, pincode);
            current_location = pincode + " " + locality;
        }
    }
}
