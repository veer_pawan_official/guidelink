package com.itwebtechnique.guidelink.view;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.Utils.AlertUtils;
import com.itwebtechnique.guidelink.app.MyApplication;
import com.itwebtechnique.guidelink.model.SuccessAllCategories;
import com.itwebtechnique.guidelink.network.Api;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Registration extends BaseActivity {
    @Inject
    Retrofit retrofit;
    public AwesomeValidation awesomeValidation;
    SuccessAllCategories return_success_AllCategories_message;
    int user_type;
    @BindView(R.id.et_first_name)
    EditText et_first_name;

    @BindView(R.id.et_last_name)
    EditText et_last_name;

    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;

    @BindView(R.id.et_email)
    EditText et_email;


    @BindView(R.id.et_password)
    EditText et_password;

    @BindView(R.id.et_cpassword)
    EditText et_cpassword;

    @BindView(R.id.customer_type)
    RadioGroup customer_type;

    @BindView(R.id.rd_terms)
    RadioButton rd_terms;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        initials();
    }

    private void initials() {
        ((MyApplication) getApplication()).getNetComponent().inject(this);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.et_email, Patterns.EMAIL_ADDRESS, R.string.emailerror);
        awesomeValidation.addValidation(this, R.id.et_first_name, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.et_last_name, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameerror);

    }

    @OnClick(R.id.btn_login)
    public void onButtonClick(View view) {
        Intent i = new Intent(getApplicationContext(), Login.class);
        startActivity(i);
    }


    @OnCheckedChanged({R.id.rd_customer, R.id.rd_business_partner})
    public void onRadioButtonCheckChanged(CompoundButton button, boolean checked) {
        if (checked) {
            switch (button.getId()) {
                case R.id.rd_customer:
                    user_type = 1;
                    break;
                case R.id.rd_business_partner:
                    user_type = 2;
                    break;
            }
        }
    }

    @OnClick(R.id.btn_sign_in)
    public void onButtonClick1(View view) {






        Intent i=new Intent(getApplicationContext(),HomeCategoriesGrid.class);
        startActivity(i);

        /*String first_name = et_first_name.getText().toString().trim();
        String last_name = et_last_name.getText().toString().trim();
        String mobile_number = et_mobile_number.getText().toString().trim();
        String email_id = et_email.getText().toString().trim();
        String password = et_password.getText().toString().trim();
        String cpassword = et_cpassword.getText().toString().trim();
        if (isNetworkAvailable()) {
            if (awesomeValidation.validate()) {

                if (validate()) {

                    userSignUp(user_type, first_name, last_name, mobile_number, email_id, password, cpassword);
                }
            }
        }
*/

    }


    private void userSignUp(int user_type, String first_name, String last_name, String mobile_number, String email_id, String password, String cpassword) {
        showProgressDialog();
        Api api = retrofit.create(Api.class);
        Call<SuccessAllCategories> call = api.user_registration(user_type, first_name, last_name, mobile_number, email_id, password, cpassword);
        call.enqueue(new Callback<SuccessAllCategories>() {
            @Override
            public void onResponse(@NonNull Call<SuccessAllCategories> call, @NonNull Response<SuccessAllCategories> response) {
                hideProgressDialog();
                return_success_AllCategories_message = response.body();
                if (response.code() == 200) {
                    if (return_success_AllCategories_message != null) {
                        if (return_success_AllCategories_message.getSuccess() == 1) {


                            openAlertDaialog();
                        } else {
                            AlertUtils.showToast(Registration.this, return_success_AllCategories_message.getMsg());
                        }
                    } else {
                        AlertUtils.showToast(Registration.this, response.message());
                    }
                } else {
                    AlertUtils.showToast(Registration.this, response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SuccessAllCategories> call, Throwable t) {
                AlertUtils.showToast(Registration.this, return_success_AllCategories_message.getMsg());
                hideProgressDialog();
            }
        });

    }

    private void openAlertDaialog() {


        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.custom_alert_registration, null);
        final Button btn_ok = (Button) alertLayout.findViewById(R.id.btn_ok);
        android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(Registration.this);
        alert.setView(alertLayout);


        btn_ok.setOnClickListener(view -> {

            Intent i = new Intent(getApplicationContext(), Login.class);
            startActivity(i);
            finish();

        });

        android.support.v7.app.AlertDialog dialog = alert.create();
        dialog.show();


    }

    private boolean validate() {
        boolean temp = true;
        String password = et_password.getText().toString().trim();
        String cpassword = et_cpassword.getText().toString().trim();

        if (!password.equals(cpassword)) {
            AlertUtils.showToast(Registration.this, "Password Not matching");

            temp = false;
        }
        if (customer_type.getCheckedRadioButtonId() == -1) {
            AlertUtils.showToast(Registration.this, "Please check user type");

            temp = false;
        }
        if (!rd_terms.isChecked()) {
            AlertUtils.showToast(Registration.this, "Please agree terms and condition!!");

            temp = false;
        }
        return temp;
    }
}
