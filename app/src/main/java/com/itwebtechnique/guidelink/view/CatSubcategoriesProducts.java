package com.itwebtechnique.guidelink.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.Utils.PaginationScrollListener;
import com.itwebtechnique.guidelink.adapter.CatSubcategoriesProductsAdapter;
import com.itwebtechnique.guidelink.adapter.PaginationAdapter;
import com.itwebtechnique.guidelink.app.MyApplication;
import com.itwebtechnique.guidelink.model.MInnerSubCategories;
import com.itwebtechnique.guidelink.model.SuccessInnerSubCategories;
import com.itwebtechnique.guidelink.viewmodel.RemoteData;
import com.itwebtechnique.guidelink.Utils.AlertUtils;
import com.itwebtechnique.guidelink.Utils.RecyclerItemClickListener;
import com.itwebtechnique.guidelink.model.MSubCategories;
import com.itwebtechnique.guidelink.model.ProductDetailOfSubCategory;
import com.itwebtechnique.guidelink.model.SuccessProductsOfSubCategory;
import com.itwebtechnique.guidelink.network.Api;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CatSubcategoriesProducts extends BaseActivity {
    private static final int PAGE_START = 1;
    @Inject
    Retrofit retrofit;
    @BindView(R.id.edt_doctor_search)
    EditText edt_search;
    @BindView(R.id.rl_subcategory)
    RelativeLayout rl_subcategory;
    @BindView(R.id.rl_newspaper_language)
    RelativeLayout rl_newspaper_language;
    @BindView(R.id.rl_inner_subcategory)
    RelativeLayout rl_inner_subcategory;
    @BindView(R.id.tv_current_location)
    TextView tv_current_location;
    @BindView(R.id.ll_header)
    LinearLayout ll_header;
    @BindView(R.id.ll_main_search_header)
    LinearLayout ll_main_search_header;
    @BindView(R.id.ll_search_location_distance)
    LinearLayout ll_search_loc_distance;
    Spinner sp_sub_categories, sp_inner_sub_category;
    Spinner sp_distance, sp_newspaper_language;
    SuccessProductsOfSubCategory mainCategories;
    List<ProductDetailOfSubCategory> subCategories;
    List<ProductDetailOfSubCategory> subCategoriesProduct = new ArrayList<>();
    ImageView img_category_banner;
    List<MSubCategories> sub_categories = new ArrayList<>();
    ArrayList<String> sub_categories_values = new ArrayList<>();
    ArrayList<Integer> sub_categories_ids = new ArrayList<Integer>();
    ArrayList<Integer> sub_categories_inner_values = new ArrayList<Integer>();
    ArrayList<String> inner_sub_categories_values = new ArrayList<>();
    ArrayList<Integer> inner_sub_categories_ids = new ArrayList<Integer>();
    String str_edt_search = "";
    SuccessInnerSubCategories successInnerSubCategories;
    List<MInnerSubCategories> mInnerSubCategories;
    String[] array_news_language;
    String regionalLanguage;
    PaginationAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    private CatSubcategoriesProductsAdapter mAdapter;
    private int category_id;
    private TextView errorText;
    private ProgressBar loader;
    private int sub_category_id, subcategory_inner_id, inner_sub_categories_id;
    private String sub_category_name;
    private String distance_in_km = "5";
    private String location = HomeCategoriesGrid.current_location;
    private AutoCompleteTextView storeTV;
    private RecyclerView recyclerView;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private int TOTAL_PAGES = 11;
    private int currentPage = PAGE_START;


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current state
        // Check carefully what you're adding into the savedInstanceState before saving it
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category_products);
        ButterKnife.bind(this);
        initials();
        adapter = new PaginationAdapter(this);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

    /*    recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                Toast.makeText(getApplicationContext(), currentPage + "", Toast.LENGTH_LONG).show();

                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage(category_id, sub_category_id, inner_sub_categories_id, location, distance_in_km, str_edt_search, regionalLanguage);
                    }
                }, 2000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
*/

        if (isNetworkAvailable()) {
            fetchSubCategories(category_id, sub_category_id, inner_sub_categories_id, location, distance_in_km, str_edt_search, regionalLanguage);
        } else {

            errorText.setVisibility(VISIBLE);
            errorText.setText(R.string.error_string);
        }


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                       // Toast.makeText(getApplicationContext(), position + "", Toast.LENGTH_LONG).show();

                        if (category_id == 23) {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" + subCategoriesProduct.get(position).getPhoneNo()));//change the number
                            startActivity(callIntent);

                        } else if (category_id == 4) {
                            String url = subCategoriesProduct.get(position).getWebsiteURL();

                            CatSubcategoriesProducts.this.sendDataToUri(url);

                        } else if (category_id == 27) {

                            String url = subCategoriesProduct.get(position).getWebsiteURL();
                            CatSubcategoriesProducts.this.sendDataToUri(url);


                        } else {

                            Intent i1 = new Intent(CatSubcategoriesProducts.this.getApplicationContext(), ProductDescription.class);
                            i1.putExtra("position", position);
                            i1.putExtra("category_id", category_id);
                            i1.putExtra("mobile_number", subCategoriesProduct.get(position).getMobileNo());
                            i1.putExtra("fax_number", subCategoriesProduct.get(position).getFaxNumber());
                            i1.putExtra("email_id", subCategoriesProduct.get(position).getEmailAddress());
                            i1.putExtra("website_url", subCategoriesProduct.get(position).getWebsiteURL());
                            i1.putExtra("address", subCategoriesProduct.get(position).getAddress());
                            i1.putExtra("location", subCategoriesProduct.get(position).getLocation());
                            i1.putExtra("latitude", subCategoriesProduct.get(position).getLatitude());
                            i1.putExtra("longitude", subCategoriesProduct.get(position).getLongitude());
                            i1.putExtra("logo", subCategoriesProduct.get(position).getLogo());
                            i1.putExtra("shop_name", subCategoriesProduct.get(position).getShopName());
                            i1.putExtra("full_name", subCategoriesProduct.get(position).getFullName());
                            i1.putExtra("post_id", subCategoriesProduct.get(position).getPostId());
                            //i1.putParcelableArrayListExtra("arr_subcategories", (ArrayList<? extends Parcelable>) subCategories);
                            CatSubcategoriesProducts.this.startActivity(i1);
                        }

                    }
                })
        );
        sp_distance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                distance_in_km = item.replaceAll("[^0-9]", "");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sp_sub_categories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sub_category_id = sub_categories_ids.get(position);
                subcategory_inner_id = sub_categories_inner_values.get(position);

                if (subcategory_inner_id == 1) {

                    getInnerSubCategories(category_id, sub_category_id);
                    rl_inner_subcategory.setVisibility(View.VISIBLE);

                } else {

                    rl_inner_subcategory.setVisibility(View.GONE);


                }

                if (sub_categories_ids.get(position) == 155) {

                    ll_search_loc_distance.setVisibility(View.VISIBLE);
                } else if (category_id == 4) {
                    ll_search_loc_distance.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        sp_inner_sub_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                inner_sub_categories_id = inner_sub_categories_ids.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        storeTV.setOnItemClickListener((parent, view, position, id) -> {

            location = parent.getItemAtPosition(position).toString();

            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        });

        sp_newspaper_language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(),position+"",Toast.LENGTH_LONG).show();
                if (position == 1) {
                    regionalLanguage = "de";
                }
                if (position == 2) {
                    regionalLanguage = "fr    ";
                }
                if (position == 3) {
                    regionalLanguage = "it";
                }
                if (position == 4) {
                    regionalLanguage = "ro";
                }
                if (position == 5) {
                    regionalLanguage = "en";
                }
                if (position == 6) {
                    regionalLanguage = "ru";
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void sendDataToUri(String url) {

        Uri webpage = Uri.parse(url);

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            webpage = Uri.parse("http://" + url);
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


    @SuppressLint("SetTextI18n")
    public void initials() {

        ((MyApplication) getApplication()).getNetComponent().inject(this);
        Toolbar toolbar = findViewById(R.id.main_header);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }


        RemoteData remoteData = new RemoteData(this);
        remoteData.getStoreData(retrofit);
        sp_sub_categories = findViewById(R.id.sp_sub_categories);
        sp_inner_sub_category = findViewById(R.id.sp_inner_sub_categories);
        errorText = findViewById(R.id.error_text);
        loader = findViewById(R.id.feed_loading);
        recyclerView = findViewById(R.id.recyclerview_subcategory_products);
        img_category_banner = findViewById(R.id.img_category_banner);
        sp_distance = findViewById(R.id.sp_distance);
        sp_newspaper_language = findViewById(R.id.sp_newspaper_language);
        storeTV = findViewById(R.id.suggest_locations);
        sp_sub_categories = findViewById(R.id.sp_sub_categories);
        tv_current_location.setText(getString(R.string.current_location) + "" + HomeCategoriesGrid.current_location);


        Intent i = getIntent();
        category_id = i.getIntExtra("category_id", 0);
        sub_category_id = i.getIntExtra("sub_category_id", 0);
        subcategory_inner_id = i.getIntExtra("sub_category_inner_id", 0);
        sub_category_name = i.getStringExtra("sub_category_name");
        String category_banner = i.getStringExtra("category_banner");
        location = HomeCategoriesGrid.current_location;

        Glide.with(getApplicationContext())
                .load(category_banner).transition(DrawableTransitionOptions.withCrossFade())
                .apply(new RequestOptions().override(600, 200))
                .into(img_category_banner);


        setValuesAccordingCategories();


        if (!(i.getStringExtra("sub_category").equals("0"))) {
            sub_categories = i.getParcelableArrayListExtra("sub_categories_list");
            rl_subcategory.setVisibility(View.VISIBLE);
            setSpinnerAdapter(sub_categories);

        }


    }

    private void setValuesAccordingCategories() {

        switch (category_id) {
            case 4:
                ll_search_loc_distance.setVisibility(View.GONE);
                edt_search.setHint(R.string.news_title);
                edt_search.setVisibility(View.VISIBLE);
                rl_newspaper_language.setVisibility(View.VISIBLE);
                setNewsPaperLanguageAdapter();
                break;
            case 5:
                edt_search.setHint(R.string.search_by_doctor_clinic_or_hospital_name);
                edt_search.setVisibility(View.VISIBLE);
                break;
            case 11:
                edt_search.setHint(R.string.search_by_shop_name);
                edt_search.setVisibility(View.VISIBLE);
                break;
            case 13:
                edt_search.setHint(R.string.search_by_shop_name);
                edt_search.setVisibility(View.VISIBLE);
                break;
            case 26:
                edt_search.setHint(R.string.search_by_shop_name);
                edt_search.setVisibility(View.VISIBLE);
                break;

            case 23:
                ll_main_search_header.setVisibility(View.GONE);
                break;
            case 27:
                ll_search_loc_distance.setVisibility(View.GONE);
            case 28:
                ll_search_loc_distance.setVisibility(View.GONE);
                break;
        }


    }

    private void setNewsPaperLanguageAdapter() {
        array_news_language = getResources().getStringArray(R.array.arr_newspaper_language);
        ArrayAdapter<String> adapterLivingSpaceMax = new ArrayAdapter<>(this,
                R.layout.spinner_item, array_news_language);
        sp_newspaper_language.setAdapter(adapterLivingSpaceMax);
    }


    private void setSpinnerAdapterView(List<MInnerSubCategories> mInnerSubCategories) {


        inner_sub_categories_values = new ArrayList<>();
        inner_sub_categories_ids = new ArrayList<>();
        for (int i = 0; i < mInnerSubCategories.size(); i++) {


            if (mInnerSubCategories.get(i).getSubCatName() == null) {
                inner_sub_categories_values.add(mInnerSubCategories.get(i).getAlt_subCatName());
            } else {

                inner_sub_categories_values.add(mInnerSubCategories.get(i).getSubCatName());
            }

        }
        for (int i = 0; i < mInnerSubCategories.size(); i++) {

            inner_sub_categories_ids.add(mInnerSubCategories.get(i).getId());

        }


        final ArrayAdapter<String> inner_sub_categories = new ArrayAdapter<String>(this, R.layout.spinner_item, inner_sub_categories_values);
        inner_sub_categories.setDropDownViewResource(R.layout.spinner_item);


        sp_inner_sub_category.setAdapter(inner_sub_categories);
        sp_inner_sub_category.setPrompt("Please select SubCategory");


    }

    @OnClick(R.id.btn_custom_search)
    public void onButtonClick(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
        } catch (Exception ignored) {
        }


        str_edt_search = edt_search.getText().toString().trim();
        //Toast.makeText(getApplicationContext(), "Cat  " + category_id + "   subcat " + sub_category_id + "loc " + location + "    inner  " + inner_sub_categories_id + "  dis  " + distance_in_km + "  editetxt  " + str_edt_search, Toast.LENGTH_LONG).show();
        subCategoriesProduct.clear();


        adapter.notifyDataSetChanged();

        fetchSubCategories(category_id, sub_category_id, inner_sub_categories_id, location, distance_in_km, str_edt_search, regionalLanguage);
    }


    private void setSpinnerAdapter(List<MSubCategories> sub_categories) {

        sub_categories_values = new ArrayList<>();
        sub_categories_ids = new ArrayList<>();
        sub_categories_inner_values = new ArrayList<>();
        for (int i = 0; i < sub_categories.size(); i++) {

            sub_categories_values.add(sub_categories.get(i).getSubCatName());

        }
        for (int i = 0; i < sub_categories.size(); i++) {

            sub_categories_ids.add(sub_categories.get(i).getId());

        }

        for (int i = 0; i < sub_categories.size(); i++) {

            sub_categories_inner_values.add(sub_categories.get(i).getIsInnerSubCategoryAvailbale());

        }

        final ArrayAdapter<String> vennue = new ArrayAdapter<String>(this, R.layout.spinner_item, sub_categories_values) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == sub_categories_values.size()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }

        };


        vennue.setDropDownViewResource(R.layout.spinner_item);


        if (category_id == 17) {
            vennue.add("Select Restaurant");
        } else {
            vennue.add("Any");
        }
        sub_categories_ids.add(0);
        sub_categories_inner_values.add(0);
        sp_sub_categories.setAdapter(vennue);
        sp_sub_categories.setSelection(vennue.getCount()); //set the hint the default selection so it appears on launch.

    }

    private void getInnerSubCategories(int category_id, int sub_category_id) {


        // showProgressDialog();


        Api api = retrofit.create(Api.class);
        Call<SuccessInnerSubCategories> call = api.getAllInnerSubCategories(category_id, sub_category_id);
        call.enqueue(new Callback<SuccessInnerSubCategories>() {
            @Override
            public void onResponse(@NonNull Call<SuccessInnerSubCategories> call, @NonNull Response<SuccessInnerSubCategories> response) {
                hideProgressDialog();

                successInnerSubCategories = response.body();

                if (response.code() == 200) {
                    if (successInnerSubCategories != null) {

                        if (successInnerSubCategories.getSuccess() == 1) {
                            if (response.body() != null) {
                                mInnerSubCategories = response.body().getInfoData();
                            }
                            if (mInnerSubCategories != null) {
                                setSpinnerAdapterView(mInnerSubCategories);
                            } else {
                                AlertUtils.showToast(CatSubcategoriesProducts.this, "No Product found!!!");
                            }


                        } else {
                            AlertUtils.showToast(CatSubcategoriesProducts.this, mainCategories.getMsg());

                        }
                    } else {
                        AlertUtils.showToast(CatSubcategoriesProducts.this, response.message());
                    }
                } else {
                    AlertUtils.showToast(CatSubcategoriesProducts.this, response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SuccessInnerSubCategories> call, @NonNull Throwable t) {
                AlertUtils.showToast(CatSubcategoriesProducts.this, t.getMessage());
                //hideProgressDialog();

            }
        });


    }

    public void fetchSubCategories(int category_id, int sub_category_id, int inner_sub_categories_id, String current_location, String distance_in_km, String edittext_search, String regionalLanguage) {


        //Toast.makeText(getApplicationContext(),"Cat  "+category_id+"   subcat "+sub_category_id+"    inner  "+inner_sub_categories_id+"     loc  "+current_location+"  dis  "+distance_in_km+"  editetxt  "+edittext_search,Toast.LENGTH_LONG).show();
        //showProgressDialog();

        loader.setVisibility(VISIBLE);
        Api api = retrofit.create(Api.class);
        Call<SuccessProductsOfSubCategory> call = api.getAllSubCategories(category_id, sub_category_id, inner_sub_categories_id,/*"5200 Brugg"*/current_location /*"5236 Remigen"*/, distance_in_km, edittext_search, regionalLanguage/*,1,20*/);
        call.enqueue(new Callback<SuccessProductsOfSubCategory>() {
            @Override
            public void onResponse(@NonNull Call<SuccessProductsOfSubCategory> call, @NonNull Response<SuccessProductsOfSubCategory> response) {
                hideProgressDialog();

                mainCategories = response.body();
                loader.setVisibility(GONE);
                errorText.setVisibility(GONE);
                if (response.code() == 200) {
                    if (mainCategories != null) {

                        if (mainCategories.getSuccess() == 1) {
                            if (response.body() != null) {
                                subCategories = response.body().getData().getSubCategories();

                            }
                            if (subCategories != null) {
                                setAdapterView();
                            } else {
                                AlertUtils.showToast(CatSubcategoriesProducts.this, "No Product found!!!");
                            }


                        } else {
                            AlertUtils.showToast(CatSubcategoriesProducts.this, mainCategories.getMsg());

                        }
                    } else {
                        AlertUtils.showToast(CatSubcategoriesProducts.this, response.message());
                    }
                } else {
                    AlertUtils.showToast(CatSubcategoriesProducts.this, response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SuccessProductsOfSubCategory> call, @NonNull Throwable t) {
                AlertUtils.showToast(CatSubcategoriesProducts.this, t.getMessage());

                //hideProgressDialog();
                loader.setVisibility(GONE);
                errorText.setVisibility(VISIBLE);
                errorText.setText(R.string.error_string);
            }
        });
    }

    private void setAdapterView() {
        mAdapter = new CatSubcategoriesProductsAdapter(CatSubcategoriesProducts.this, subCategories, category_id);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        addAllProducts(subCategories);
        //Toast.makeText(getApplicationContext(),subCategories.get(0).getAddress()+"",Toast.LENGTH_LONG).show();
       /* addAllProducts(subCategories);
        adapter.addAll(subCategories, category_id);
*/
        /*if (currentPage <= TOTAL_PAGES)
            //adapter.addLoadingFooter();
        else isLastPage = true;*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadNextPage(int category_id, int sub_category_id, int inner_sub_categories_id, String current_location, String distance_in_km, String edittext_search, String regionalLanguage) {
        loader.setVisibility(VISIBLE);
        Api api = retrofit.create(Api.class);
        Call<SuccessProductsOfSubCategory> call = api.getAllSubCategories(category_id, sub_category_id, inner_sub_categories_id,/*"5200 Brugg"*/current_location /*"5236 Remigen"*/, distance_in_km, edittext_search, regionalLanguage/*,1,20*/);
        call.enqueue(new Callback<SuccessProductsOfSubCategory>() {
            @Override
            public void onResponse(@NonNull Call<SuccessProductsOfSubCategory> call, @NonNull Response<SuccessProductsOfSubCategory> response) {
                hideProgressDialog();

                mainCategories = response.body();
                loader.setVisibility(GONE);
                errorText.setVisibility(GONE);
                if (response.code() == 200) {
                    if (mainCategories != null) {

                        if (mainCategories.getSuccess() == 1) {
                            if (response.body() != null) {
                                subCategories = response.body().getData().getSubCategories();
                            }
                            if (subCategories != null) {


                                adapter.removeLoadingFooter();
                                isLoading = false;
                                addAllProducts(subCategories);
                                adapter.addAll(subCategories, category_id);


                               /* if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                                else isLastPage = true;*/
                            } else {

                                AlertUtils.showToast(CatSubcategoriesProducts.this, "No Product found!!!");
                            }


                        } else {
                            AlertUtils.showToast(CatSubcategoriesProducts.this, mainCategories.getMsg());

                        }
                    } else {
                        AlertUtils.showToast(CatSubcategoriesProducts.this, response.message());
                    }
                } else {
                    AlertUtils.showToast(CatSubcategoriesProducts.this, response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SuccessProductsOfSubCategory> call, @NonNull Throwable t) {
                AlertUtils.showToast(CatSubcategoriesProducts.this, t.getMessage());

                //hideProgressDialog();
                loader.setVisibility(GONE);
                errorText.setVisibility(VISIBLE);
                errorText.setText(R.string.error_string);
            }
        });
    }

    public void add(ProductDetailOfSubCategory r) {
        subCategoriesProduct.add(r);

    }

    public void addAllProducts(List<ProductDetailOfSubCategory> moveResults) {

        for (ProductDetailOfSubCategory result : moveResults) {
            add(result);
        }
    }
}
