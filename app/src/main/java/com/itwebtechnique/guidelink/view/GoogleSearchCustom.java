package com.itwebtechnique.guidelink.view;

import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.adapter.GoogleSearchCustomAdapter;
import com.itwebtechnique.guidelink.model.GoogleResults;
import com.itwebtechnique.guidelink.model.GoogleResultsItems;
import com.itwebtechnique.guidelink.network.Api;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GoogleSearchCustom extends BaseActivity {
    @BindView(R.id.edt_search)
    EditText edt_search;
    // Your API key
    // TODO replace with your value
    String key = "AIzaSyBdsSNbovzMfRTmCs6vXpsTjRO3ibafnSs";

    // Your Search Engine ID
    // TODO replace with your value
    String cx = "002217694701841391745:g3q76bpiuou";
    GoogleResults googleResults;
    List<GoogleResultsItems> googleResultsItems;
    private GoogleSearchCustomAdapter mAdapter;
    private TextView errorText;
    private ProgressBar loader;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_search_custom);
        ButterKnife.bind(this);
        initials();

    }

    private void initials() {
        errorText = findViewById(R.id.error_text);
        loader = findViewById(R.id.feed_loading);
        recyclerView = findViewById(R.id.recyclerview);
    }


    @OnClick(R.id.btn_go)
    public void onButtonClick(View view) {
        String searchQuery = edt_search.getText().toString().trim();
        getCustomResults(searchQuery, key, cx, "json");
    }

    private void getCustomResults(String searchQuery, String key, String cx, String jsonFormat) {


       //Toast.makeText(getApplicationContext(),searchQuery+"      "+key+cx+"        "+jsonFormat,Toast.LENGTH_LONG).show();
        loader.setVisibility(View.VISIBLE);
        showProgressDialog();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.googleapis.com/customsearch/")
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();



        Api apiService = retrofit.create(Api.class);
        Call<GoogleResults> call = apiService.getResultFromGoogle(searchQuery, key, cx, jsonFormat);
        call.enqueue(new Callback<GoogleResults>() {
            @Override
            public void onResponse(@NonNull Call<GoogleResults> call, @NonNull Response<GoogleResults> response) {
                loader.setVisibility(View.INVISIBLE);
                hideProgressDialog();


                    googleResults = response.body();

                if (googleResults != null) {
                    googleResultsItems = googleResults.getItesm();
                   // Toast.makeText(getApplicationContext(), googleResultsItems.get(0).getTitle() + "", Toast.LENGTH_SHORT).show();

                    setAdapterView(googleResultsItems);
                }else{

                    Toast.makeText(getApplicationContext(), response.message() + "", Toast.LENGTH_SHORT).show();

                }




            }

            @Override
            public void onFailure(@NonNull Call<GoogleResults> call, @NonNull Throwable t) {
                loader.setVisibility(View.INVISIBLE);
                hideProgressDialog();
                Toast.makeText(getApplicationContext(), t + "", Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void setAdapterView(List<GoogleResultsItems> googleResultsItemss) {
        mAdapter = new GoogleSearchCustomAdapter(GoogleSearchCustom.this, googleResultsItemss);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }

}
