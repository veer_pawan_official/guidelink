package com.itwebtechnique.guidelink.network;

import com.itwebtechnique.guidelink.model.GoogleResults;
import com.itwebtechnique.guidelink.model.SuccessAllCategories;
import com.itwebtechnique.guidelink.model.SuccessInnerSubCategories;
import com.itwebtechnique.guidelink.model.SuccessLocationSuggetion;
import com.itwebtechnique.guidelink.model.SuccessProductsOfSubCategory;
import com.itwebtechnique.guidelink.model.SuccessSubCategories;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @FormUrlEncoded
    @POST("forgotpass-guidelink")
    Call<SuccessAllCategories> sendPasswordLing(@Field("loginEmail") String email_address);

    @FormUrlEncoded
    @POST("login-guidelink")
    Call<SuccessAllCategories> user_login(@Field("loginEmail") String email,
                                          @Field("loginPassword") String password);

    @FormUrlEncoded
    @POST("signup-guidelink")
    Call<SuccessAllCategories> user_registration(@Field("UserType") int user_type,
                                                 @Field("FirstName") String first_name,
                                                 @Field("LastName") String last_name,
                                                 @Field("MobileNo") String mobile_number,
                                                 @Field("EmailAddress") String email_id,
                                                 @Field("Password") String password,
                                                 @Field("CPassword") String cpassword);

    @POST("category-all")
    Call<SuccessAllCategories> getAllCategories();

    @FormUrlEncoded
    @POST("categoryPostedDetail")
    Call<SuccessProductsOfSubCategory> getAllSubCategories(
            @Field("CategoryId") int id,
            @Field("subCategory") int subCategoryId,
            @Field("innerSubCategory") int innerSubCategoriesId,
            @Field("Location") String user_location,
            @Field("distanceKm") String distance_in_km,
            @Field("searchName") String string_search,
            @Field("newsLang") String regional_language/*,
            @Field("page") int page,
            @Field("pageSize") int pageSize*/
    );


    @FormUrlEncoded
    @POST("subCategory-all")
    Call<SuccessSubCategories> getSubCategories(@Field("CategoryId") int category_id);

    @POST("location-suggestion")
    Call<SuccessLocationSuggetion> getStoreData();

    @GET("v1")
    Call<GoogleResults> getResultFromGoogle(@Query("q") String searchQuesry,
                                            @Query("key") String key,
                                            @Query("cx") String cx,
                                            @Query("alt") String jsonFormat);

    @FormUrlEncoded
    @POST("subCategory-Innerall")
    Call<SuccessInnerSubCategories> getAllInnerSubCategories(@Field("CategoryId") int category_id,
                                                             @Field("subCategory") int sub_category_id);


}
