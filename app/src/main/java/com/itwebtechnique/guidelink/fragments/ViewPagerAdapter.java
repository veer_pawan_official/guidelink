package com.itwebtechnique.guidelink.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Priyabrat on 21-08-2015.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment =new PassengersCar();
        }
        else if (position == 1)
        {
            fragment = new CommercialVehicle();
        }
        else if (position == 2)
        {
            fragment = new Truck();
        }
        if (position == 3)
        {
            fragment = new MobileHomeCar();
        }
        else if (position == 4)
        {
            fragment = new Pendant();
        }
        else if (position == 5)
        {
            fragment =new MotorCycle();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "PassengersCar";
        }
        else if (position == 1)
        {
            title = "CommercialVehicle";
        }
        else if (position == 2)
        {
            title = "Truck";
        }
        if (position == 3)
        {
            title = "MobileHomeCar";
        }
        else if (position == 4)
        {
            title = "Pendant";
        }
        else if (position == 5)
        {
            title = "MotorCycle";
        }
        return title;
    }
}
