package com.itwebtechnique.guidelink.app;

import android.app.Application;

import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.di.ApiComponent;
import com.itwebtechnique.guidelink.di.ApiModule;
import com.itwebtechnique.guidelink.di.AppModule;
import com.itwebtechnique.guidelink.di.DaggerApiComponent;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MyApplication extends Application {

    private ApiComponent mApiComponent;

    @Override
    public void onCreate() {
        super.onCreate();


        mApiComponent = DaggerApiComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule("https://www.guidelink.ch/demo/marketing/api/"))
                .build();

    }
    public ApiComponent getNetComponent() {
        return mApiComponent;
    }
}
