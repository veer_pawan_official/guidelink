package com.itwebtechnique.guidelink.viewmodel;

import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.itwebtechnique.guidelink.Utils.DialogsUtils;
import com.itwebtechnique.guidelink.model.SuccessAllCategories;

public class successViewModel extends ViewModel {

    SuccessAllCategories return_success_AllCategories_message;
    private MutableLiveData<SuccessAllCategories> success_message;
    ProgressDialog pDialog;

    public LiveData<SuccessAllCategories> getVerificationLink(String email_address, Context applicationContext) {
        if (success_message == null) {
            success_message = new MutableLiveData<SuccessAllCategories>();
            pDialog= DialogsUtils.showProgressDialog(applicationContext,"Please wait...");



        }

        return success_message;
    }


}
