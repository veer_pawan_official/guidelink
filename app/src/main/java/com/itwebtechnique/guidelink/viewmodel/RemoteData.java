package com.itwebtechnique.guidelink.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.itwebtechnique.guidelink.R;
import com.itwebtechnique.guidelink.Utils.AlertUtils;
import com.itwebtechnique.guidelink.model.MLocationSuggestion;
import com.itwebtechnique.guidelink.model.SuccessLocationSuggetion;
import com.itwebtechnique.guidelink.network.Api;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RemoteData {

    private Context context;

    public RemoteData(Context contextIn){
        context = contextIn;

    }

    public void getStoreData(Retrofit retrofit) {


        Api api = retrofit.create(Api.class);
        Call<SuccessLocationSuggetion> call = api.getStoreData();
        call.enqueue(new Callback<SuccessLocationSuggetion>() {
            @Override
            public void onResponse(@NonNull Call<SuccessLocationSuggetion> call,
                                   @NonNull Response<SuccessLocationSuggetion> response) {


             /*   Log.d("Async Data RemoteData",
                        "Got REMOTE DATA " + response.body().getLoationsuggestion());
*/
                List<String> str = new ArrayList<String>();
             if(response.body() != null) {
                 for (MLocationSuggestion s : response.body().getLoationsuggestion()) {
                     str.add(s.getPostecodeCity());
                 }

                 AutoCompleteTextView storeTV = ((Activity) context).findViewById(R.id.suggest_locations);
                 ArrayAdapter<String> adapteo = new ArrayAdapter<String>(context,
                         android.R.layout.simple_dropdown_item_1line, str.toArray(new String[0]));
                 storeTV.setAdapter(adapteo);
             }else {
                 AlertUtils.showToast(context,"No location suggestions!!");
             }
            }

            @Override
            public void onFailure(@NonNull Call<SuccessLocationSuggetion> call, @NonNull Throwable t) {
               /* Log.e("Async Data RemoteData",
                        "error in getting remote data");*/
                AlertUtils.showToast(context,"No location suggestions!!");
            }
        });
    }
}