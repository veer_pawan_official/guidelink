package com.itwebtechnique.guidelink.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Toast;


import com.itwebtechnique.guidelink.Utils.AlertUtils;
import com.itwebtechnique.guidelink.model.SuccessAllCategories;

import com.itwebtechnique.guidelink.network.Api;
import com.itwebtechnique.guidelink.view.HomeCategoriesGrid;


import java.util.Observable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class LoginViewModel extends Observable {


    private SuccessAllCategories return_success_AllCategories_message;


    private Context context;
    public ObservableInt progressBar;
    public final ObservableField<String > username = new ObservableField<>("");
    public final ObservableField<String > userpass = new ObservableField<>("");

    public LoginViewModel(Context context)
    {

        this.context = context;
        progressBar = new ObservableInt(View.GONE);


    }


    public void sendLoginRequest(String name, String pass, Retrofit retrofit)
    {
        progressBar.set(View.VISIBLE);
        Api api = retrofit.create(Api.class);
        //Api apiService = ApiClient.getClient().create(Api.class);
        Call<SuccessAllCategories> loginresponse = api.user_login(name,pass);
        loginresponse.enqueue(new Callback<SuccessAllCategories>() {
            @Override
            public void onResponse(@NonNull Call<SuccessAllCategories> call, @NonNull Response<SuccessAllCategories> response) {

                progressBar.set(View.GONE);
                return_success_AllCategories_message = response.body();
                if (response.code() == 200) {
                    if (return_success_AllCategories_message != null) {
                        if (return_success_AllCategories_message.getSuccess() == 1) {
                            Intent i = new Intent(context, HomeCategoriesGrid.class);
                            context.startActivity(i);

                        } else {
                            AlertUtils.showToast(context, return_success_AllCategories_message.getMsg());
                        }
                    } else {
                        AlertUtils.showToast(context, response.message());
                    }
                } else {
                    AlertUtils.showToast(context, response.message());
                }

            }

            @Override
            public void onFailure(@NonNull Call<SuccessAllCategories> call, @NonNull Throwable t) {
                progressBar.set(View.GONE);
                showToast(""+t.getMessage());

            }
        });




    }





    void showToast(String msg)
    {

        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();

    }

}
