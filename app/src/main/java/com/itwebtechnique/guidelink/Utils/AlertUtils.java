package com.itwebtechnique.guidelink.Utils;

import android.content.Context;
import android.service.autofill.UserData;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.itwebtechnique.guidelink.R;


public class AlertUtils {

    public static void showToast(final Context context, int message) {
        if (context != null) {
            showCustomToast(context, context.getString(message));
        }
    }

    public static void showToast(final Context context, String message) {
        if (context != null) {
            showCustomToast(context, message);
        }
    }

    private static void showCustomToast(final Context context, String message) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            View toastLayout = inflater.inflate(R.layout.layout_custom_toast, null);
            TextView text = toastLayout.findViewById(R.id.tv_toast);
            text.setText(message);
            Toast toast = new Toast(context);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(toastLayout);
            toast.show();
        }
    }
}
