package com.itwebtechnique.guidelink.Utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class Utils {

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 100;

    public static boolean isNetworkAvailable(Context context) {
        boolean isConnected = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            isConnected = netInfo != null && netInfo.isConnectedOrConnecting();
        }
        return isConnected;
    }


    public static void hideKeyBoard(final Activity activity) {
        if (activity != null) {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }


    public static String getAppVersion(Context context) {
        String versionCode = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionCode = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }





/*    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }*/

    public static String getImageNameFromUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            return url.substring(url.lastIndexOf('/') + 1);
        } else {
            return "";
        }
    }

    public static String formatDate(String dateString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
            Date date = format.parse(dateString);
            SimpleDateFormat format2 = new SimpleDateFormat("MMMM dd, yyyy hh:mma", Locale.US);
            return format2.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return dateString;
        }
    }

    public static String formatDate2(String dateString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
            Date date = format.parse(dateString);
            SimpleDateFormat format2 = new SimpleDateFormat("dd MMMM yyyy, hh:mma", Locale.US);
            return format2.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return dateString;
        }
    }

    public static String formatDateForNotifications(String dateString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
            Date date = format.parse(dateString);
            SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy, hh:mm a", Locale.US);
            return format2.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return dateString;
        }
    }

    public static String formatSurveyDate(String dateString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date date = format.parse(dateString);
            SimpleDateFormat format2 = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
            return format2.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return dateString;
        }
    }


    public static Date getTitleFightDate(String dateString) {
        try {
//            06-08-2018 1:30 AM
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
            return format.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
